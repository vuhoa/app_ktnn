/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
// import Home from './src/components/Home';
import Navigators from './src/components/Navigators';
// import DanhsachCuocHop from './src/components/DanhsachCuocHop';
import {name as appName} from './app.json';
console.disableYellowBox = true;
AppRegistry.registerComponent(appName, () => Navigators);
