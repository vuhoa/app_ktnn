import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';
import {Styles} from '../config/Styles';
export default class ChiTietThongTinCanBo extends Component {
  render() {
    var {params} = this.props.navigation.state;

    return (
      <View style={[Styles.container]}>
        
        <View style={[Styles.wrapper, {marginVertical: 15}]}>
          <View style={[Styles.flexTitle, Styles.bgTitle]}>
            <Text style={[Styles.styleContent, {flex: 2}]}>Họ và tên: </Text>
            <Text style={[Styles.styleNameCb, {flex: 6}]}>
              {params.item.name}{' '}
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              flexDirection: 'row',
              marginVertical: 25,
            }}>
            <Image
              source={params.item.image}
              style={{height: 175, width: 138, borderRadius: 5}}
              resizeMode="contain"></Image>
          </View>
          <View style={Styles.itemList1}>
            <View style={Styles.flexRow}>
              <Text style={[Styles.styleContent, {flex: 2}]}>Ngày sinh:</Text>
              <Text style={[Styles.styleLabel, {flex: 6}]}>
                {params.item.ngaysinh}{' '}
              </Text>
            </View>
          </View>
          <View style={Styles.itemList}>
            <View style={Styles.flexRow}>
              <Text style={[Styles.styleContent, {flex: 2}]}>Số hiệu:</Text>
              <Text style={[Styles.styleLabel, {flex: 6}]}>
                {params.item.sohieu}{' '}
              </Text>
            </View>
          </View>
          <View style={Styles.itemList1}>
            <View style={Styles.flexRow}>
              <Text style={[Styles.styleContent, {flex: 2}]}>Chức vụ:</Text>
              <Text style={[Styles.styleLabel, {flex: 6}]}>
                {params.item.chucvu}{' '}
              </Text>
            </View>
          </View>
          <View style={Styles.itemList}>
            <View style={Styles.flexRow}>
              <Text style={[Styles.styleContent, {flex: 2}]}>Đơn vị:</Text>
              <Text style={[Styles.styleLabel, {flex: 6}]}>
                {params.item.tendonvi}{' '}
              </Text>
            </View>
          </View>
          <View style={Styles.itemList1}>
            <View style={Styles.flexRow}>
              <Text style={[Styles.styleContent, {flex: 2}]}>Phòng ban: </Text>
              <Text style={[Styles.styleLabel, {flex: 6}]}>
                {params.item.phongban}{' '}
              </Text>
            </View>
          </View>
          <View style={Styles.itemList}>
            <View style={Styles.flexRow}>
              <Text style={[Styles.styleContent, {flex: 2}]}>Biên chế: </Text>
              <Text style={[Styles.styleLabel, {flex: 6}]}>
                {params.item.bienche}
              </Text>
            </View>
          </View>
          <View style={Styles.itemList1}>
            <View style={Styles.flexRow}>
              <Text style={[Styles.styleContent, {flex: 2}]}>Điện thoại: </Text>
              <Text style={[Styles.styleLabel, {flex: 6}]}>
                {params.item.phone}{' '}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
