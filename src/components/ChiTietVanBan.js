import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';
import {Styles} from '../config/Styles';

export default class ChiTietVanBan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animation: new Animated.Value(0),
    };
  }
  toggleOpen = () => {
    const toValue = this._open ? 0 : 1;
    Animated.timing(this.state.animation, {
      toValue,
      duration: 200,
    }).start();
    this._open = !this._open;
  };
  static navigationOptions = {
    headerTitle: 'Chi tiết văn bản',
  };
  render() {
    const bgStyle = {
      transform: [
        {
          scale: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 30],
          }),
        },
      ],
    };
    const baocaoStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -60],
          }),
        },
      ],
    };
    const themmoiStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -115],
          }),
        },
      ],
    };
    const canhanStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -170],
          }),
        },
      ],
    };
    const labelPositionInterpolate = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -240],
    });
    const opacityInterpolate = this.state.animation.interpolate({
      inputRange: [0, 0.9, 1],
      outputRange: [0, 0, 1],
    });
    const labelStyle = {
      opacity: opacityInterpolate,
      transform: [
        {
          translateX: labelPositionInterpolate,
        },
      ],
    };
    var {params} = this.props.navigation.state;
    return (
      <View style={[Styles.container]}>
        <Animated.View style={[Styles.Overlay, bgStyle]} />
        <ScrollView>
          <View style={[Styles.wrapper, {marginVertical: 15}]}>
            <View style={[Styles.flexRow, Styles.bgTitle, {marginBottom: 15}]}>
              <Text style={Styles.nameCV}>{params.name}</Text>
            </View>
            <View>
              <View style={Styles.itemList1}>
                <Text style={Styles.listTitle}>Trích yếu</Text>
                <View style={[Styles.flexRow, Styles.space1]}>
                  <Text style={Styles.styleContent}>
                    Test dữ liệu văn bản đến năm 2019
                  </Text>
                </View>
              </View>
              <View style={Styles.itemList}>
                <Text style={[Styles.listTitle, {paddingVertical: 4}]}>
                  Tệp đính kèm
                </Text>
                <View style={[Styles.flexRow, Styles.space1]}>
                  <Image
                    source={require('../images/pdf.png')}
                    style={{width: 15, height: 19, marginRight: 10}}
                    resizeMode="contain"></Image>
                  <Text style={Styles.styleNumber1}>2373.PDF</Text>
                </View>
              </View>
              <View style={Styles.itemList1}>
                <Text style={[Styles.listTitle, {paddingVertical: 4}]}>
                  Tên cơ quan ban hành
                </Text>
                <View style={[Styles.flexRow, Styles.space1]}>
                  <Text style={Styles.styleContent1}>Vụ pháp chế</Text>
                </View>
              </View>
              <View style={Styles.itemList}>
                <Text style={[Styles.listTitle, {paddingVertical: 4}]}>
                  Ngày ban hành
                </Text>
                <View style={[Styles.flexRow, Styles.space1]}>
                  <Text style={Styles.styleContent}>13/11/2019</Text>
                </View>
              </View>
              <View style={Styles.itemList1}>
                <Text style={[Styles.listTitle, {paddingVertical: 4}]}>
                  Ý kiến chỉ đạo
                </Text>
                <View style={[Styles.flexRow, Styles.space1]}>
                  <Text style={Styles.styleNumber1}>Lãnh đạo xử lý: </Text>
                  <Text style={Styles.styleContent}>
                    Giám đốc: Phạm Thị Thu Hà
                  </Text>
                </View>
              </View>
              <View style={Styles.itemList}>
                <Text style={[Styles.listTitle, {paddingVertical: 4}]}>
                  Hạn xử lý
                </Text>
                <View style={[Styles.flexRow, Styles.space1]}>
                  <Text style={Styles.styleContent}>13/11/2019</Text>
                </View>
              </View>
              <View style={Styles.itemList1}>
                <Text style={[Styles.listTitle, {paddingVertical: 4}]}>
                  Trạng thái xử lý
                </Text>
                <View style={[Styles.flexRow, Styles.space1]}>
                  <Text style={Styles.styleNumber1}>Chưa xử lý</Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, canhanStyle]}>
            <TouchableOpacity>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                Cập nhật TT cá nhân
              </Animated.Text>
              <Image
                source={require('../images/chitietvb_canhan.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, themmoiStyle]}>
            <TouchableOpacity>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                Cập nhật TT văn bản
              </Animated.Text>
              <Image
                source={require('../images/chitietvb_vb.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, baocaoStyle]}>
            <TouchableOpacity>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                Xử lý văn bản
              </Animated.Text>
              <Image
                source={require('../images/chitietvb_xuly.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={this.toggleOpen}>
          <View style={Styles.buttonCreat}>
            <Image
              source={require('../images/icon_fix.png')}
              style={{width: 70, height: 70, zIndex: 1}}
              resizeMode="cover"></Image>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
