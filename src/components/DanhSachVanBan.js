import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  Picker,
  Image,
  Button,
  TouchableOpacity,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import {Styles} from '../config/Styles';
import DatePicker from 'react-native-datepicker';
import LinearGradient from 'react-native-linear-gradient';
import listVbChuaXuLy from '../data/listVbChuaXuly';
console.disableYellowBox = true;
class BoxShadowBottom extends Component {
  render() {
    return (
      <LinearGradient
        colors={['#d2d2d2', '#fbfbfb']}
        style={Styles.shadow}></LinearGradient>
    );
  }
}
class VanBanItem extends Component {
  // const {navigate} = this.props.navigation;
  render() {
    return (
      <View style={{marginBottom: 10}}>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('ChiTietVanBan', {
              name: this.props.item.name,
            })
          }>
          <View style={[Styles.flexRow, Styles.boxTitleCv]}>
            {this.props.item.isRead == 0 ? (
              <LinearGradient
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={['#00adef', '#005aab']}
                style={Styles.dot}></LinearGradient>
            ) : (
              <View style={[Styles.circleMenu, {marginRight: 5}]}></View>
            )}
            <Text style={Styles.nameCV}>{this.props.item.name}</Text>
          </View>
        </TouchableOpacity>

        <View style={Styles.detailCv}>
          <Text style={Styles.styleContent1}>
            {this.props.item.description}
          </Text>
          <View style={[Styles.flexRow]}>
            <View style={[Styles.flexRow]}>
              <Image
                source={require('../images/xemvb.png')}
                style={Styles.iconView}></Image>
              <Text style={[Styles.styleNumber1, {fontStyle: 'italic'}]}>
                Xem văn bản
              </Text>
            </View>
            <View style={[Styles.flexRow]}>
              <Image
                source={require('../images/xulyvb.png')}
                style={Styles.iconView}></Image>
              <Text style={[Styles.styleNumber1, {fontStyle: 'italic'}]}>
                Xử lý văn bản
              </Text>
            </View>
            <View style={[Styles.flexRow]}>
              <Image
                source={require('../images/hanxl.png')}
                style={Styles.iconView}></Image>
              <Text style={[Styles.styleNumber1, {fontStyle: 'italic'}]}>
                Hạn xử lý
              </Text>
            </View>
          </View>
        </View>
        <BoxShadowBottom></BoxShadowBottom>
      </View>
    );
  }
}
export default class DanhSachVanBan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dateStart: new Date(),
      dateEnd: new Date(),
      itemsCount: 3,
      currentDate: new Date(),
    };

    // var date = new Date().getDate(); //Current Date
    // var month = new Date().getMonth() + 1; //Current Month
    // var year = new Date().getFullYear(); //Current Year
    // this.setState({
    //   //Setting the value of the date time
    //   dateStart: date + '-' + month + '-' + year,
    //   dateEnd: date + '-' + month + '-' + year,
    // });
  }
  // componentDidMount() {
  //   var that = this;

  //   var date = new Date().getDate(); //Current Date
  //   var month = new Date().getMonth() + 1; //Current Month
  //   var year = new Date().getFullYear(); //Current Year
  //   var hours = new Date().getHours(); //Current Hours
  //   that.setState({
  //     //Setting the value of the date time
  //     dateStart: date + '-' + month + '-' + year,
  //     dateEnd: date + '-' + month + '-' + year,
  //   });
  // }

  render() {
    const navigate = this.props.navigation;
    return (
      <View style={[Styles.container, Styles.wrapper]}>
        <View style={Styles.sectionHeader}>
          <View style={Styles.wrapperContainerInput}>
            <TouchableWithoutFeedback>
              <View style={Styles.inputContainer}>
                <Text style={Styles.styleLabelInput}>Từ khóa</Text>
                <TextInput
                  style={Styles.styleInput}
                  placeholder="Tìm kiếm"
                  placeholderTextColor="#989898"></TextInput>
              </View>
            </TouchableWithoutFeedback>
            <View style={Styles.inputContainer}>
              <Text style={Styles.styleLabelInput}>Cơ quan ban hành</Text>
              <View style={Styles.styleSelect}>
                <Picker
                  style={{backgroundColor: 'transparent'}}
                  //   selectedValue='Cơ quan ban hành'

                  // onValueChange={(itemValue, itemIndex) =>
                  //   this.setState({language: itemValue})
                  // }
                >
                  <Picker.Item label="" value="" />
                  <Picker.Item label="" value="" />
                </Picker>
                <Image
                  source={require('../images/down.png')}
                  style={Styles.pickerIcon}
                  resizeMode="contain"></Image>
              </View>
            </View>
          </View>
          <View style={Styles.wrapperContainerInput}>
            <View style={Styles.inputContainer}>
              <Text style={Styles.styleLabelInput}>Ngày bắt đầu</Text>
              <DatePicker
                style={[Styles.styleInput, {width: undefined}]}
                date={this.state.dateStart}
                mode="date"
                format="DD-MM-YYYY"
                // minDate="2016-05-01"
                // maxDate="2016-06-01"
                // confirmBtnText="Confirm"
                // cancelBtnText="Cancel"
                iconSource={require('../images/icon_date.png')}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    right: 0,
                    top: 4,
                    marginLeft: 0,
                    height: 15,
                    width: 15,
                  },
                  dateInput: {
                    marginLeft: 0,
                    borderWidth: 0,
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    marginTop: 2,
                  },
                }}
                onDateChange={date => {
                  this.setState({dateStart: date});
                }}
              />
            </View>
            <View style={Styles.inputContainer}>
              <Text style={Styles.styleLabelInput}>Ngày kết thúc</Text>
              <DatePicker
                style={[Styles.styleInput, {width: undefined}]}
                date={this.state.dateEnd}
                mode="date"
                format="DD-MM-YYYY"
                iconSource={require('../images/icon_date.png')}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    right: 0,
                    top: 4,
                    marginLeft: 0,
                    height: 15,
                    width: 15,
                  },
                  dateInput: {
                    marginLeft: 0,
                    borderWidth: 0,
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'stretch',
                    marginTop: 2,
                  },
                }}
                onDateChange={date => {
                  this.setState({dateEnd: date});
                }}
              />
            </View>
          </View>
          <TouchableOpacity
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 32,
              marginBottom: 10,
            }}
            onPress={() => {
              // this.props.navigation.navigate('Home');
            }}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#00adef', '#005aab']}
              style={Styles.searchButton}>
              <Text style={Styles.textSearch}>Tìm kiếm</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
        <BoxShadowBottom></BoxShadowBottom>
        {/* <View> */}
        <FlatList
          data={listVbChuaXuLy.slice(0, this.state.itemsCount)}
          style={{marginTop: 10}}
          renderItem={({item, index}) => {
            return (
              <VanBanItem
                item={item}
                index={index}
                navigation={this.props.navigation}></VanBanItem>
            );
          }}></FlatList>
      </View>
      // </View>
    );
  }
}
