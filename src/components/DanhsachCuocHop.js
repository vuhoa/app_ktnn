import React, {Component} from 'react';
import {Text, View, Image, FlatList} from 'react-native';
import {Styles} from '../config/Styles';
import danhsachcuochopData from '../data/danhsachcuochopData';
import HeaderTab from './HeaderTab';
import LinearGradient from 'react-native-linear-gradient';

class BoxShadowBottom extends Component {
  render() {
    return (
      <LinearGradient
        colors={['#d2d2d2', '#fbfbfb']}
        style={Styles.shadow}></LinearGradient>
    );
  }
}
class CuocHopItem extends Component {
  render() {
    return (
      <View style={{marginBottom: 10}}>
        <View style={[Styles.flexRow, Styles.boxTitleCv]}>
          <Image
            source={require('../images/icon_dscuochop.png')}
            style={{width: 14, height: 13, marginRight: 8}}
            resizeMode="contain"></Image>

          <Text style={Styles.nameCV}>{this.props.item.name}</Text>
        </View>
        <View style={[Styles.detailCv, {paddingLeft: 30}]}>
          <View style={[Styles.flexRow, Styles.space]}>
            <Text style={Styles.styleLabel}>Thời gian:</Text>
            <Text style={Styles.styleContent}>{this.props.item.thoigian}</Text>
          </View>
          <View style={[Styles.flexRow, Styles.space]}>
            <Text style={Styles.styleLabel}>Địa điểm:</Text>
            <Text style={Styles.styleContent}>{this.props.item.diadiem}</Text>
          </View>
          <View style={[Styles.flexRow, Styles.space]}>
            <Text style={Styles.styleLabel}>Chủ trì:</Text>
            <Text style={Styles.styleContent}>{this.props.item.chutri}</Text>
          </View>
        </View>
        <BoxShadowBottom></BoxShadowBottom>
      </View>
    );
  }
}
export default class DanhsachCuocHop extends Component {
  render() {
    return (
      <View style={Styles.container}>
        {/* <HeaderTab
              name="Văn bản chưa xử lý"
              navigation={this.props.navigation}></HeaderTab> */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
            marginVertical: 15,
          }}>
          <Text style={{color: '#0b5492', fontSize: 12}}>Tuần trước</Text>
          <View style={{flexDirection: 'column', alignItems: 'center'}}>
            <Text style={{color: '#000', fontSize: 12}}>Tuần 44</Text>
            <Text style={{color: '#000', fontSize: 12}}>
              Từ ngày 27/10/2019 - 03/11/2019
            </Text>
          </View>
          <Text style={{color: '#0b5492', fontSize: 12}}>Tuần tiếp theo</Text>
        </View>
        <View style={{marginHorizontal: 20}}>
          <BoxShadowBottom></BoxShadowBottom>
        </View>
        <View style={[Styles.wrapper, {marginVertical: 20}]}>
          <FlatList
            data={danhsachcuochopData}
            renderItem={({item, index}) => {
              return <CuocHopItem item={item} index={index}></CuocHopItem>;
            }}></FlatList>
        </View>
      </View>
    );
  }
}
