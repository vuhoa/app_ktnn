import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  Button,
} from 'react-native';
import {Styles} from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';
class BoxShadowBottom extends Component {
  render() {
    return (
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#fbda70', '#f5eaaa']}
        style={{height: 2}}></LinearGradient>
    );
  }
}
export default class HeaderTab extends Component<props> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <ImageBackground
          source={require('../images/bg_header.png')}
          style={{height: 50}}
          resizeMode="cover">
          <View style={Styles.wrapperHeader}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Home');
              }}>
              <Image
                source={require('../images/back.png')}
                style={{height: 18, width: 30, marginLeft: 5}} resizeMode="contain"></Image>
            </TouchableOpacity>
            <Text style={Styles.headerTextTitle}>{this.props.name}</Text>
            <View style={Styles.rightTitle}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ThongBao');
                }}>
                <Image
                  source={require('../images/icon_noti.png')}
                  style={{height: 25, width: 20, marginRight: 15}}
                  resizeMode="contain"></Image>
                  <View style={Styles.dotNoti}></View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ThongBao');
                }}>
                <Image
                  source={require('../images/icon_user.png')}
                  style={{height: 25, width: 25, marginRight: 15}}
                  resizeMode="contain"></Image>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
        <BoxShadowBottom></BoxShadowBottom>
      </View>
    );
  }
}
