import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  Button,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import HeaderTab from './HeaderTab';
import {Styles} from '../config/Styles';
import {VictoryPie} from 'victory-native';

export default class HoatDongKT extends Component {
  static navigationOptions = ({navigation}) => {
    let tabBarLabel = 'Kiểm toán';
    let tabBarIcon = () => (
      <Image
        source={require('../images/hdkt.png')}
        style={{width: 22, height: 14}}></Image>
    );
    return {tabBarLabel, tabBarIcon};
  };
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      modalVisible: true,
      animation: new Animated.Value(0),
    };
  }
  showHideComponents = () => {
    this.setState(state => ({
      active: !state.active,
    }));
  };
  toggleOpen = () => {
    const toValue = this._open ? 0 : 1;
    Animated.timing(this.state.animation, {
      toValue,
      duration: 400,
    }).start();
    this._open = !this._open;
  };
  render() {
    const bgStyle = {
      transform: [
        {
          scale: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 30],
          }),
        },
      ],
    };
    const baocaoStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -60],
          }),
        },
      ],
    };
    const themmoiStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -115],
          }),
        },
      ],
    };
    const labelPositionInterpolate = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -240],
    });
    const opacityInterpolate = this.state.animation.interpolate({
      inputRange: [0, 0.9, 1],
      outputRange: [0, 0, 1],
    });
    const labelStyle = {
      opacity: opacityInterpolate,
      transform: [
        {
          translateX: labelPositionInterpolate,
        },
      ],
    };
    return (
      <View style={Styles.containerDetail}>
        <Animated.View style={[Styles.Overlay, bgStyle]} />
        <HeaderTab
          name="Hoạt động kiểm toán"
          navigation={this.props.navigation}></HeaderTab>
        <ScrollView contentContainerStyle={{paddingVertical: 12}}>
          <View style={Styles.wrapper}>
            <View style={Styles.wrapperTitle}>
              <Text style={Styles.styleTitle}>Tổng hợp kết quả kiểm toán</Text>
              <View style={Styles.line}></View>
            </View>
            <View>
              <View style={[Styles.flexRow, Styles.bgTitle]}>
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 0}}
                  colors={['#00adef', '#005aab']}
                  style={Styles.dot}></LinearGradient>

                <Text style={Styles.nameCV}>Số liệu năm 2019</Text>
              </View>
              <View style={Styles.detailCv}>
                <View style={[Styles.flexRow, Styles.space]}>
                  <Text style={[Styles.styleLabel1]}>
                    Tổng hợp số liệu kiến nghị kiểm toán
                  </Text>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <Text style={Styles.styleLabel}>Tổng số: </Text>
                  <Text style={Styles.styleNumber}>35,172,934,988,706 VNĐ</Text>
                </View>
              </View>
            </View>
            <TouchableOpacity>
              <View style={Styles.itemList}>
                <View style={Styles.boxlistTitle}>
                  <Text style={Styles.listTitle}>Ngân sách nhà nước</Text>
                  <Image
                    source={require('../images/arrow.png')}
                    style={{width: 14, height: 10}}></Image>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <Text style={Styles.styleLabel}>Tổng số: </Text>
                  <Text style={Styles.styleNumber1}>
                    34,685,814,213,499 VNĐ
                  </Text>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <View
                    style={[
                      Styles.circle,
                      {backgroundColor: '#3266cc'},
                    ]}></View>
                  <Text style={Styles.styleNumber1}>98,62 %</Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={Styles.itemList1}>
                <View style={Styles.boxlistTitle}>
                  <Text style={Styles.listTitle}>Chuyên đề, chương trình</Text>
                  <Image
                    source={require('../images/arrow.png')}
                    style={{width: 14, height: 10}}></Image>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <Text style={Styles.styleLabel}>Tổng số: </Text>
                  <Text style={Styles.styleNumber1}>399,690,977,034 VNĐ</Text>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <View
                    style={[
                      Styles.circle,
                      {backgroundColor: '#db3912'},
                    ]}></View>
                  <Text style={Styles.styleNumber1}>1,14 %</Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={Styles.itemList}>
                <View style={Styles.boxlistTitle}>
                  <Text style={Styles.listTitle}>Đầu tư dự án</Text>
                  <Image
                    source={require('../images/arrow.png')}
                    style={{width: 14, height: 10}}></Image>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <Text style={Styles.styleLabel}>Tổng số: </Text>
                  <Text style={Styles.styleNumber1}>87,429,798,173 VNĐ</Text>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <View
                    style={[
                      Styles.circle,
                      {backgroundColor: '#fe9900'},
                    ]}></View>
                  <Text style={Styles.styleNumber1}>0,52 %</Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={Styles.itemList1}>
                <View style={Styles.boxlistTitle}>
                  <Text style={Styles.listTitle}>
                    Quốc phòng - An ninh - Đảng
                  </Text>
                  <Image
                    source={require('../images/arrow.png')}
                    style={{width: 14, height: 10}}></Image>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <Text style={Styles.styleLabel}>Tổng số: </Text>
                  <Text style={Styles.styleNumber1}>0 VNĐ</Text>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <View
                    style={[
                      Styles.circle,
                      {backgroundColor: '#079c13'},
                    ]}></View>
                  <Text style={Styles.styleNumber1}>0,00 %</Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={Styles.itemList}>
                <View style={Styles.boxlistTitle}>
                  <Text style={Styles.listTitle}>Kiểm toán doanh nghiệp</Text>
                  <Image
                    source={require('../images/arrow.png')}
                    style={{width: 14, height: 10}}></Image>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <Text style={Styles.styleLabel}>Tổng số: </Text>
                  <Text style={Styles.styleNumber1}>0 VNĐ</Text>
                </View>
                <View style={[Styles.flexRow, Styles.space]}>
                  <View
                    style={[
                      Styles.circle,
                      {backgroundColor: '#990099'},
                    ]}></View>
                  <Text style={Styles.styleNumber1}>0,00 %</Text>
                </View>
              </View>
            </TouchableOpacity>
            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
              <VictoryPie
               width={300}
               height={300}
                colorScale={['#3266cc', '#db3912', '#fe9900']}
                data={[{y: 98.62}, {y: 1.14}, {y: 0.52}]}
                labels={() => null}
              />
            </View>
          </View>
        </ScrollView>
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, themmoiStyle]}>
            <Animated.Text style={[Styles.textToggle, labelStyle]}>
              Thực hiện kiến nghị KT
            </Animated.Text>
            <Image
              source={require('../images/hdkt_kiennghi.png')}
              style={{width: 46, height: 46}}></Image>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, baocaoStyle]}>
            <Animated.Text style={[Styles.textToggle, labelStyle]}>
              Theo dõi tiến độ
            </Animated.Text>
            <Image
              source={require('../images/hdkt_tiendo.png')}
              style={{width: 46, height: 46}}></Image>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={this.toggleOpen}>
          <View style={Styles.buttonCreat}>
            <Image
              source={require('../images/icon_fix.png')}
              style={{width: 70, height: 70, zIndex: 1}}
              resizeMode="cover"></Image>
          </View>
        </TouchableWithoutFeedback>
        {/* <View style={Styles.iconFix}>
          {this.state.active ? (
            <View style={Styles.toggleBox}>
              <TouchableOpacity
                onPress={() => {}}
                style={[Styles.flexRow, Styles.alignCenter]}>
                <Text style={Styles.textToggle}>Thực hiện kiến nghị KT</Text>
                <Image
                  source={require('../images/themmoicv.png')}
                  style={{width: 36, height: 36}}></Image>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {}}
                style={[Styles.flexRow, Styles.alignCenter]}>
                <Text style={Styles.textToggle}>Theo dõi tiến độ</Text>
                <Image
                  source={require('../images/baocaocv.png')}
                  style={{width: 36, height: 36}}></Image>
              </TouchableOpacity>
            </View>
          ) : null}
          <TouchableOpacity
            onPress={() => {
              this.showHideComponents();
            }}
            style={Styles.buttonCreat}>
            <Image
              source={require('../images/icon_fix.png')}
              style={{width: 70, height: 70}}></Image>
          </TouchableOpacity>
        </View> */}
      </View>
    );
  }
}
