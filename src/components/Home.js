import React, {Component} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Styles} from '../config/Styles';

export default class Home extends Component<Props> {
  constructor(props) {
    super(props);
  }
  static navigationOptions = ({navigation}) => {
    let headerTransparent = true;
    let headerLeft = (
      <TouchableOpacity
        onPress={() => {
          navigation.openDrawer();
        }}>
        <Image
          source={require('../images/menu_bar.png')}
          style={{height: 17, width: 17, marginLeft: 15}}></Image>
      </TouchableOpacity>
    );
    let headerStyle = {
      borderBottomWidth: 0,
    };

    return {headerStyle, headerTransparent, headerLeft};
  };

  render() {
    const navigation = this.props.navigation;
    return (
      <ImageBackground
        source={require('../images/bg.png')}
        style={{width: '100%', height: '100%'}} resizeMode="cover">
        <View style={Styles.container}>
          <View style={Styles.top}>
            <Image
              source={require('../images/logo1.png')}
              style={{height: 95, width: 95, marginBottom: 10}}></Image>
            <Image
              source={require('../images/logo.png')}
              style={{width: 286, height: 55, resizeMode: 'contain'}}></Image>
          </View>
          <View style={Styles.down}>
            <View style={Styles.boxDown}>
              <View style={Styles.boxItem}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('QuanLyVanBan')}>
                  <Image
                    source={require('../images/icon_qlvb.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Quản lý</Text>
                  <Text style={Styles.textName}>văn bản</Text>
                </TouchableOpacity>
              </View>
              <View style={Styles.boxItem}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('QuanLyCongViec')}>
                  <Image
                    source={require('../images/icon_qlcv.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Quản lý</Text>
                  <Text style={Styles.textName}>công việc</Text>
                </TouchableOpacity>
              </View>
              <View style={Styles.boxItem}>
                <TouchableOpacity onPress={() => navigation.navigate('HoatDongKT')}>
                  <Image
                    source={require('../images/icon_hdkt.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Hoạt động</Text>
                  <Text style={Styles.textName}>kiểm toán</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={Styles.boxDown}>
              <View style={Styles.boxItem}>
                <TouchableOpacity  onPress={() => navigation.navigate('QuanLyCanBo')}>
                  <Image
                    source={require('../images/icon_ttcb.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Thông tin</Text>
                  <Text style={Styles.textName}>cán bộ</Text>
                </TouchableOpacity>
              </View>
              <View style={Styles.boxItem}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('QuanLyCuocHop')}>
                  <Image
                    source={require('../images/icon_qlch.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Quản lý </Text>
                  <Text style={Styles.textName}>cuộc họp</Text>
                </TouchableOpacity>
              </View>
              <View style={Styles.boxItem}>
                <TouchableOpacity onPress={() => navigation.navigate('TTCanBo')}>
                  <Image
                    source={require('../images/icon_vbpl.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Văn bản </Text>
                  <Text style={Styles.textName}>pháp luật</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={Styles.boxDown}>
              <View style={Styles.boxItem}>
                <TouchableOpacity>
                  <Image
                    source={require('../images/icon_tl.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Thảo luận</Text>
                </TouchableOpacity>
              </View>
              <View style={Styles.boxItem}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('QuanLyCuocHop')}>
                  <Image
                    source={require('../images/icon_tt.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Tin tức</Text>
                </TouchableOpacity>
              </View>
              <View style={Styles.boxItem}>
                <TouchableOpacity onPress={() => {navigation.navigate('ThongBao')}}>
                  <Image
                    source={require('../images/thongbao.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Thông báo </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={Styles.boxDown}>
              <View style={Styles.boxItem}>
                <TouchableOpacity>
                  <Image
                    source={require('../images/icon_dt.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.textName}>Đào tạo</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
