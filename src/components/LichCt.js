import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  Animated,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import {Styles} from '../config/Styles';
import CalendarStrip from 'react-native-calendar-strip';
import moment from 'moment';
// import 'moment';
// import 'moment/locale/fr';  // language must match config
// import moment from 'moment-timezone';  // only if timezone is needed
let datesWhitelist = [
  {
    start: moment(),
    end: moment().add(3, 'days'), // total 4 days enabled
  },
];
let datesBlacklist = [moment().add(1, 'days')]; // 1 day disabled
export default class LichCt extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultValue_dropDown: 'Tổng hợp',
      animation: new Animated.Value(0),
      weekStart: moment(),
      selectedDate: moment(),
      defaultSelect: true,
    };
  }
  toggleOpen = () => {
    const toValue = this._open ? 0 : 1;
    Animated.timing(this.state.animation, {
      toValue,
      duration: 200,
    }).start();
    this._open = !this._open;
  };

  render() {
    const bgStyle = {
      transform: [
        {
          scale: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 40],
          }),
        },
      ],
    };
    const baocaoStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -60],
          }),
        },
      ],
    };
    const themmoiStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -115],
          }),
        },
      ],
    };
    const donviStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -170],
          }),
        },
      ],
    };
    const lctthangStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -225],
          }),
        },
      ],
    };
    const labelPositionInterpolate = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -240],
    });
    const opacityInterpolate = this.state.animation.interpolate({
      inputRange: [0, 0.9, 1],
      outputRange: [0, 0, 1],
    });
    const labelStyle = {
      opacity: opacityInterpolate,
      transform: [
        {
          translateX: labelPositionInterpolate,
        },
      ],
    };
    const locale = {
      name: 'fr',
      config: {
        months: 'Tháng 01_Tháng 02_Tháng 03_Tháng 04_Tháng 05_Tháng 06_Tháng 07_Tháng 08_Tháng 09_Tháng 10_Tháng 11_Tháng 12'.split(
          '_',
        ),
        monthsShort: 'Janv_Févr_Mars_Avr_Mai_Juin_Juil_Août_Sept_Oct_Nov_Déc'.split(
          '_',
        ),
        weekdays: 'Dimanche_Lundi_Mardi_Mercredi_Jeudi_Vendredi_Samedi'.split(
          '_',
        ),
        weekdaysShort: 'CN_T2_T3_T4_T5_T6_T7'.split('_'),
        weekdaysMin: 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
        longDateFormat: {
          LT: 'HH:mm',
          LTS: 'HH:mm:ss',
          L: 'DD/MM/YYYY',
          LL: 'D MMMM YYYY',
          LLL: 'D MMMM YYYY LT',
          LLLL: 'dddd D MMMM YYYY LT',
        },
        calendar: {
          sameDay: "[Aujourd'hui à] LT",
          nextDay: '[Demain à] LT',
          nextWeek: 'dddd [à] LT',
          lastDay: '[Hier à] LT',
          lastWeek: 'dddd [dernier à] LT',
          sameElse: 'L',
        },
        relativeTime: {
          future: 'dans %s',
          past: 'il y a %s',
          s: 'quelques secondes',
          m: 'une minute',
          mm: '%d minutes',
          h: 'une heure',
          hh: '%d heures',
          d: 'un jour',
          dd: '%d jours',
          M: 'un mois',
          MM: '%d mois',
          y: 'une année',
          yy: '%d années',
        },
        ordinalParse: /\d{1,2}(er|ème)/,
        ordinal: function(number) {
          return number + (number === 1 ? 'er' : 'ème');
        },
        meridiemParse: /PD|MD/,
        isPM: function(input) {
          return input.charAt(0) === 'M';
        },
        // in case the meridiem units are not separated around 12, then implement
        // this function (look at locale/id.js for an example)
        // meridiemHour : function (hour, meridiem) {
        //     return /* 0-23 hour, given meridiem token and hour 1-12 */
        // },
        meridiem: function(hours, minutes, isLower) {
          return hours < 12 ? 'PD' : 'MD';
        },
        week: {
          dow: 1, // Monday is the first day of the week.
          doy: 4, // The week that contains Jan 4th is the first week of the year.
        },
      },
    };

    return (
      <View style={[Styles.container,{position:'relative'}]}>
        <Animated.View style={[Styles.Overlay1, bgStyle]} />
        <ScrollView>
          <View style={{marginTop: 15}}>
            <View style={Styles.topBoxSelect}>
              <Text style={Styles.styleLabelInputSelect}>Xem lịch</Text>
              <View style={Styles.styleBoxDrop}>
                <ScrollView>
                  <ModalDropdown
                    style={Styles.styleDropdown}
                    textStyle={Styles.dropdown_text}
                    defaultValue={this.state.defaultValue_dropDown}
                    dropdownStyle={Styles.dropdown_2_dropdown}
                    options={[
                      'LCT tháng của LĐKT',
                      'LCT của đơn vị',
                      'LCT tuần các đơn vị',
                    ]}></ModalDropdown>
                  <Image
                    source={require('../images/down.png')}
                    style={Styles.pickerIcon1}
                    resizeMode="contain"></Image>
                </ScrollView>
              </View>
            </View>
            {/*  lich */}
            {/* <CalendarStrip
              calendarAnimation={{type: 'parallel', duration: 30}}
              daySelectionAnimation={{
                type: 'background',
                duration: 300,
                highlightColor: '#0b5492',
                color: '#fff',
                borderRadius: 0,
              }}
              style={{height: 80, paddingTop: 5, paddingBottom: 5}}
              calendarHeaderStyle={{color: '#fff'}}
              highlightDateNumberStyle={{color: '#fff', fontSize: 8}}
              highlightDateNameStyle={{color: '#fff', fontSize: 12}}
              calendarColor={'#41abe5'}
              dateNumberStyle={{color: 'white', fontSize: 8}}
              dateNameStyle={{color: 'white', fontSize: 12}}
              calendardateNumberFormat={'dd-MM-YYYY'}
              iconLeft={require('../images/pre.png')}
              iconRight={require('../images/next.png')}
              iconContainer={{flex: 0.1}}
              locale={locale}
              onDateSelected={value =>
                this.setState({
                  selectedDate: moment(value).format('YYYY-MM-DD'),
                })
              }
              selectedDate={this.state.selectedDate}
              useIsoWeekday={true}
              startingDate={this.state.selectedDate}
              */}
            {/* /> */}
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'space-between',
                backgroundColor: '#41abe5',
                paddingHorizontal: 10,
                // paddingVertical: 10,
                alignItems: 'center',
              }}>
              <View>
                <Image
                  source={require('../images/pre.png')}
                  style={{width: 6, height: 13}}
                  resizeMode="contain"></Image>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View style={{alignItems: 'center', paddingVertical:5}}>
                  <Text
                    style={{
                      paddingHorizontal: 5,
                      fontSize: 12,
                      color: 'white',
                    }}>
                    T2
                  </Text>
                  <Text
                    style={{paddingHorizontal: 5, fontSize: 8, color: 'white'}}>
                    18/11/2019
                  </Text>
                </View>
                {this.state.defaultSelect== true ? (
                  <View style={{alignItems: 'center', backgroundColor:'#0b5492', paddingVertical:7}}>
                    <Text
                      style={{
                        paddingHorizontal: 5,
                        fontSize: 12,
                        color: 'white',
                      }}>
                      T3
                    </Text>
                    <Text
                      style={{
                        paddingHorizontal: 5,
                        fontSize: 8,
                        color: 'white',
                      }}>
                      19/11/2019
                    </Text>
                    <View style={Styles.triangle}></View>
                  </View>
                  
                ) : (
                  <View style={{alignItems: 'center', paddingVertical:5}}>
                    <Text
                      style={{
                        paddingHorizontal: 5,
                        fontSize: 12,
                        color: 'white',
                      }}>
                      T3
                    </Text>
                    <Text
                      style={{
                        paddingHorizontal: 5,
                        fontSize:8,
                        color: 'white',
                      }}>
                      19/11/2019
                    </Text>
                  </View>
                )}
              
                <View style={{alignItems: 'center', paddingVertical:5}}>
                  <Text
                    style={{
                      paddingHorizontal: 5,
                      fontSize: 12,
                      color: 'white',
                    }}>
                    T4
                  </Text>
                  <Text
                    style={{paddingHorizontal: 5, fontSize: 8, color: 'white'}}>
                    20/11/2019
                  </Text>
                </View>
                <View style={{alignItems: 'center', paddingVertical:5}}>
                  <Text
                    style={{
                      paddingHorizontal: 5,
                      fontSize: 12,
                      color: 'white',
                    }}>
                    T5
                  </Text>
                  <Text
                    style={{paddingHorizontal: 5, fontSize: 8, color: 'white'}}>
                    21/11/2019
                  </Text>
                </View>
                <View style={{alignItems: 'center', paddingVertical:5}}>
                  <Text
                    style={{
                      paddingHorizontal: 5,
                      fontSize: 12,
                      color: 'white',
                    }}>
                    T6
                  </Text>
                  <Text
                    style={{paddingHorizontal: 5, fontSize: 8, color: 'white'}}>
                    22/11/2019
                  </Text>
                </View>
                <View style={{alignItems: 'center', paddingVertical:5}}>
                  <Text
                    style={{
                      paddingHorizontal: 5,
                      fontSize: 12,
                      color: 'white',
                    }}>
                    T7
                  </Text>
                  <Text
                    style={{paddingHorizontal: 5, fontSize: 8, color: 'white'}}>
                    23/11/2019
                  </Text>
                </View>
              </View>
              <View>
                <Image
                  source={require('../images/next.png')}
                  style={{width: 6, height: 13}}
                  resizeMode="contain"></Image>
              </View>
            </View>
            {/* <CalendarStrip
                  ref={'myCalendarStrip'}
                  showMonth={false}
                  date={this.state.date} // remove this prop
                  selectedDate={this.state.selectedDate}
                  startingDate={this.state.weekStart}
                  onWeekChange={this._handleOnWeekChange}
                  onDateSelected={selectedDate => this.setState({selectedDate})}
                  daySelectionAnimation={{ type: 'background', duration: 300, highlightColor: '#7B5E90' }}
                  highlightDateNumberStyle={{ color: 'white', fontSize: 12 }}
                  highlightDateNameStyle={{ color: 'white', fontSize: 8 }}
                  maxDate={moment()}
                  minDate={moment()}
                  locale={locale}
          /> */}
            {/*  */}

            <View style={Styles.detailContent}>
              <View style={Styles.topDetailContent}>
                <View style={Styles.flexRow}>
                  <Image
                    source={require('../images/dsch.png')}
                    style={{width: 18, height: 13, marginRight: 6}}
                    resizeMode="contain"></Image>
                  <Text style={Styles.nameCV}>
                    Chương trình làm việc ngày 19/11/2019
                  </Text>
                </View>
                <Text style={Styles.styleDes}>Vụ tổ chức cán bộ</Text>
              </View>

              <View style={Styles.itemList1}>
                <Text style={Styles.listTitle}>Thời gian</Text>
                <Text style={Styles.styleContent1}>Thứ ba: 19/11/2019</Text>
                <View style={[Styles.flexRow, Styles.space]}>
                  <Text style={Styles.styleContent1}>Thời gian:</Text>
                  <Text style={[Styles.styleNumber, {marginTop: 5}]}>
                    8:00- 11:00
                  </Text>
                </View>
              </View>
              <View style={Styles.itemList}>
                <Text style={Styles.listTitle}>Nội dung công việc</Text>
                <Text style={Styles.styleContent1}>Họp với BNV</Text>
                <Text style={Styles.styleContent1}>Tài liệu họp:</Text>

                <View style={[Styles.flexRow, Styles.space]}>
                  <Image
                    source={require('../images/pdf.png')}
                    style={{width: 15, height: 19, marginRight: 7}}
                    resizeMode="contain"></Image>
                  <Text style={Styles.styleNumber1}>
                    Kế hoạch tuần từ 18/11 - 23/11/2019.doc0
                  </Text>
                </View>
              </View>
              <View style={Styles.itemList1}>
                <Text style={Styles.listTitle}>Bộ phận chuẩn bị nội dung</Text>
              </View>
              <View style={Styles.itemList}>
                <Text style={Styles.listTitle}>Thành phần</Text>
                <Text style={Styles.styleContent1}>Tổng KTNN Hồ Đức Phớc</Text>
                <Text style={Styles.styleContent1}>VPKTNN </Text>
              </View>
              <View style={Styles.itemList1}>
                <Text style={Styles.listTitle}>Địa điểm</Text>
                <Text style={Styles.styleContent1}>Thứ ba: 19/11/2019</Text>
                <View style={[Styles.flexRow, Styles.space]}>
                  <Text style={Styles.styleContent1}>Thời gian:</Text>
                  <Text style={[Styles.styleNumber, {marginTop: 5}]}>
                    8:00- 11:00
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        {/* dieu huong */}
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, lctthangStyle]}>
            <TouchableOpacity>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                LCT tháng của LĐKT
              </Animated.Text>
              <Image
                source={require('../images/lct_ldkt.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, donviStyle]}>
            <TouchableOpacity>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                LCT của đơn vị
              </Animated.Text>
              <Image
                source={require('../images/lct_donvi.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, themmoiStyle]}>
            <TouchableOpacity>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                LCT tuần các đơn vị
              </Animated.Text>
              <Image
                source={require('../images/lct_tuan.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, baocaoStyle]}>
            <TouchableOpacity>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                Cuộc họp cá nhân
              </Animated.Text>
              <Image
                source={require('../images/lct_chcanhan.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={this.toggleOpen}>
          <View style={[Styles.buttonCreat]}>
            <Image
              source={require('../images/icon_fix.png')}
              style={{width: 70, height: 70, zIndex: 1}}
              resizeMode="cover"></Image>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
