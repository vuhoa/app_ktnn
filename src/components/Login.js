import React, {Component} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Button,
  Keyboard,
} from 'react-native';
import {Styles} from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';

export default class Login extends Component<Props> {
  constructor(props) {
    super(props);
  }
  //   static navigationOptions = ({navigation}) => {
  //     let headerTransparent = true;
  //     let headerLeft = (
  //       <TouchableOpacity
  //         onPress={() => {
  //           navigation.openDrawer();
  //         }}>
  //         <Image
  //           source={require('../images/menu_bar.png')}
  //           style={{height: 17, width: 17, marginLeft: 15}}></Image>
  //       </TouchableOpacity>
  //     );
  //     let headerStyle = {
  //       borderBottomWidth: 0,
  //     };

  //     return {headerStyle, headerTransparent, headerLeft};
  //   };

  render() {
    const navigation = this.props.navigation;
    return (
      <ImageBackground
        source={require('../images/bg.png')}
        style={{width: '100%', height: '100%', resizeMode: 'contain'}}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={Styles.container}>
            <View style={Styles.topLogin}>
            <Image
              source={require('../images/logo1.png')}
              style={{height: 95, width: 95, marginBottom: 10}}></Image>
            <Image
              source={require('../images/logo.png')}
              style={{width: 286, height: 55, }} resizeMode= 'contain'></Image>
            </View>
            <View style={Styles.down}>
              <View style={Styles.textInputContainer}>
                <Image
                  source={require('../images/icon_userlg.png')}
                  style={{height: 17, width: 19}}
                  resizeMode="contain"></Image>
                <TextInput style={Styles.textInput} placeholder="Tài khoản" />
              </View>
              <View style={Styles.textInputContainer}>
                <Image
                  source={require('../images/icon_pass.png')}
                  style={{height: 17, width: 19}}
                  resizeMode="contain"></Image>
                <TextInput
                  style={Styles.textInput}
                  placeholder="Mật khẩu"
                  secureTextEntry={true}
                />
              </View>
              <TouchableOpacity
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 32,
                }}
                onPress={() => {
                  navigation.navigate('Home');
                }}>
                <LinearGradient
                  // start={{x: 0, y: 0}}
                  // end={{x: 1, y: 0}}
                  colors={['#4c669f', '#3b5998', '#192f6a']}
                  style={Styles.loginButton}>
                  <Text style={Styles.loginTitle}>Đăng nhập</Text>
                </LinearGradient>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {}} style={Styles.forgetPass}>
                <Text style={Styles.textforgetPass}>Quên mật khẩu</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ImageBackground>
    );
  }
}
