import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  ScrollView,
} from 'react-native';
import {
  createAppContainer,
  TabBarBottom,
  TabNavigator,
  createSwitchNavigator,
} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import {Transition} from 'react-native-reanimated';
import {Styles} from '../config/Styles';
import Login from '../components/Login';
import Home from './Home';
import QuanLyVanBan from './QuanLyVanBan';
import QuanLyCongViec from './QuanLyCongViec';
import QuanLyCuocHop from './QuanLyCuocHop';

import HoatDongKT from './HoatDongKT';
import ThongKeTongHop from './ThongKeTongHop';
import ThongBao from './ThongBao';
import SideMenu from './SideMenu';
import VanBanChuaXuLy from './VanBanChuaXuLy';
import DanhSachVanBan from './DanhSachVanBan';
import ChiTietVanBan from './ChiTietVanBan';
import QuanLyCanBo from './QuanLyCanBo';
import ChiTietThongTinCanBo from './ChiTietThongTinCanBo';
import LichCt from './LichCt';
import LinearGradient from 'react-native-linear-gradient';
import DanhsachCuocHop from './DanhsachCuocHop';

class BoxShadowBottom extends Component {
  render() {
    return (
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#fbda70', '#f5eaaa']}
        style={{height: 2}}></LinearGradient>
    );
  }
}
//
const mainStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        headerBackground: false,
        headerTransparent: true,
        tabBarVisible: false,
        // tabBar: ({ state }) => ({
        //   visible: false
        // })
      },
    },

    // VanBanChuaXuLy: {
    //   screen: VanBanChuaXuLy,
    //   navigationOptions: {

    //   },
    // },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      headerTitleStyle: {
        color: '#fff',
        alignSelf: 'center',
        textAlign: 'center',
        flex: 1,
        fontSize: 15,
        textTransform: 'uppercase',
        fontWeight: 'bold',
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            source={require('../images/back.png')}
            style={{height: 18, width: 30, marginLeft: 5}}
            resizeMode="contain"></Image>
        </TouchableOpacity>
      ),
      headerRight: (
        <View style={Styles.rightTitle}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ThongBao');
            }}>
            <Image
              source={require('../images/icon_noti.png')}
              style={{height: 25, width: 20, marginRight: 15}}
              resizeMode="contain"></Image>
            <View style={Styles.dotNoti}></View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Home');
            }}>
            <Image
              source={require('../images/icon_user.png')}
              style={{height: 25, width: 25, marginRight: 15}}
              resizeMode="contain"></Image>
          </TouchableOpacity>
        </View>
      ),
      headerBackground: (
        <View>
          <Image
            source={require('../images/bg_header.png')}
            style={{width: '100%', height: 50}}
          />
          <BoxShadowBottom></BoxShadowBottom>
        </View>
      ),
    }),
  },
);
const AuthStack = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      header: null,
    },
  },
});
const vanbanStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        headerBackground: false,
        headerTransparent: true,
        tabBarVisible: false,
      },
    },
    QuanLyVanBan: {
      screen: QuanLyVanBan,
      navigationOptions: {
        header: null,
      },
    },
    ThongBao: {
      screen: ThongBao,
      navigationOptions: {
        headerTitle: 'Thông báo',
      },
    },
    VanBanChuaXuLy: {
      screen: VanBanChuaXuLy,
      navigationOptions: {
        headerTitle: 'Văn bản chưa xử lý',
      },
    },
    DanhSachVanBan: {
      screen: DanhSachVanBan,
      navigationOptions: {
        headerTitle: 'Danh sách văn bản',
      },
    },
    ChiTietVanBan: {
      screen: ChiTietVanBan,
    },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      headerTitleStyle: styles.styleHeaderTitle,
      headerStyle: {
        height: 50,
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            source={require('../images/back.png')}
            style={{height: 18, width: 30, marginLeft: 5}}
            resizeMode="contain"></Image>
        </TouchableOpacity>
      ),
      headerRight: (
        <View style={Styles.rightTitle}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ThongBao');
            }}>
            <Image
              source={require('../images/icon_noti.png')}
              style={{height: 25, width: 20, marginRight: 15}}
              resizeMode="contain"></Image>
            <View style={Styles.dotNoti}></View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Home');
            }}>
            <Image
              source={require('../images/icon_user.png')}
              style={{height: 25, width: 25, marginRight: 15}}
              resizeMode="contain"></Image>
          </TouchableOpacity>
        </View>
      ),
      headerBackground: (
        <View>
          <Image
            source={require('../images/bg_header.png')}
            style={{width: '100%', height: 50}}
          />
          <BoxShadowBottom></BoxShadowBottom>
        </View>
      ),
    }),
  },
);
vanbanStack.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName

  if ( routeName == 'Home' ) {
      tabBarVisible = false
  }

  return {
      tabBarVisible,
  }
}
const congviecStack = createStackNavigator(
  {
    QuanLyCongViec: {
      screen: QuanLyCongViec,
      navigationOptions: {
        header: null,
      },
    },
    ThongBao: {
      screen: ThongBao,
      navigationOptions: {
        headerTitle: 'Thông báo',
      },
    },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      headerTitleStyle: styles.styleHeaderTitle,
      headerStyle: {
        height: 50,
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            source={require('../images/back.png')}
            style={{height: 18, width: 30, marginLeft: 5}}
            resizeMode="contain"></Image>
        </TouchableOpacity>
      ),
      headerRight: (
        <View style={Styles.rightTitle}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ThongBao');
            }}>
            <Image
              source={require('../images/icon_noti.png')}
              style={{height: 25, width: 20, marginRight: 15}}
              resizeMode="contain"></Image>
            <View style={Styles.dotNoti}></View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Home');
            }}>
            <Image
              source={require('../images/icon_user.png')}
              style={{height: 25, width: 25, marginRight: 15}}
              resizeMode="contain"></Image>
          </TouchableOpacity>
        </View>
      ),
      headerBackground: (
        <View>
          <Image
            source={require('../images/bg_header.png')}
            style={{width: '100%', height: 50}}
          />
          <BoxShadowBottom></BoxShadowBottom>
        </View>
      ),
    }),
  },
);
const cuochopStack = createStackNavigator(
  {
    QuanLyCuocHop: {
      screen: QuanLyCuocHop,
      navigationOptions: {
        header: null,
      },
    },
    ThongBao: {
      screen: ThongBao,
      navigationOptions: {
        headerTitle: 'Thông báo',
      },
    },
    LichCt: {
      screen: LichCt,
      navigationOptions: {
        headerTitle: 'LCT của lãnh đạo KT',
      },
    },
    DanhsachCuocHop: {
      screen: DanhsachCuocHop,
      navigationOptions: {
        headerTitle: 'Danh sách cuộc họp',
      },
    },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      headerTitleStyle: styles.styleHeaderTitle,
      headerStyle: {
        height: 50,
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            source={require('../images/back.png')}
            style={{height: 18, width: 30, marginLeft: 5}}
            resizeMode="contain"></Image>
        </TouchableOpacity>
      ),
      // headerBackImage: (
      //   <Image
      //     source={require('../images/back.png')}
      //     style={{height: 18, width: 9, marginLeft: 15}}></Image>
      // ),
      headerRight: (
        <View style={Styles.rightTitle}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ThongBao');
            }}>
            <Image
              source={require('../images/icon_noti.png')}
              style={{height: 25, width: 20, marginRight: 15}}
              resizeMode="contain"></Image>
            <View style={Styles.dotNoti}></View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Home');
            }}>
            <Image
              source={require('../images/icon_user.png')}
              style={{height: 25, width: 25, marginRight: 15}}
              resizeMode="contain"></Image>
          </TouchableOpacity>
        </View>
      ),
      headerBackground: (
        <View>
          <Image
            source={require('../images/bg_header.png')}
            style={{width: '100%', height: 50}}
          />
          <BoxShadowBottom></BoxShadowBottom>
        </View>
      ),
    }),
  },
);
const canboStack = createStackNavigator(
  {
    QuanLyCanBo: {
      screen: QuanLyCanBo,
      navigationOptions: {
        header: null,
      },
    },
    ThongKeTongHop: {
      screen: ThongKeTongHop,
      navigationOptions: {
        headerTitle: 'Thống kê tổng hợp',
      },
    },
    ChiTietThongTinCanBo: {
      screen: ChiTietThongTinCanBo,
      navigationOptions: {
        headerTitle: 'Thông tin cán bộ chi tiết ',
      },
    },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      headerTitleStyle: styles.styleHeaderTitle,
      headerStyle: {
        height: 50,
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            source={require('../images/back.png')}
            style={{height: 18, width: 30, marginLeft: 5}}
            resizeMode="contain"></Image>
        </TouchableOpacity>
      ),
      headerRight: (
        <View style={Styles.rightTitle}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ThongBao');
            }}>
            <Image
              source={require('../images/icon_noti.png')}
              style={{height: 25, width: 20, marginRight: 15}}
              resizeMode="contain"></Image>
            <View style={Styles.dotNoti}></View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Home');
            }}>
            <Image
              source={require('../images/icon_user.png')}
              style={{height: 25, width: 25, marginRight: 15}}
              resizeMode="contain"></Image>
          </TouchableOpacity>
        </View>
      ),
      headerBackground: (
        <View>
          <Image
            source={require('../images/bg_header.png')}
            style={{width: '100%', height: 50}}
          />
          <BoxShadowBottom></BoxShadowBottom>
        </View>
      ),
    }),
  },
);
const kiemtoanStack = createStackNavigator(
  {
    HoatDongKT: {
      screen: HoatDongKT,
      navigationOptions: {
        header: null,
      },
    },
    ThongBao: {
      screen: ThongBao,
      navigationOptions: {
        headerTitle: 'Thông báo',
      },
    },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      headerTitleStyle: styles.styleHeaderTitle,
      headerStyle: {
        height: 50,
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            source={require('../images/back.png')}
            style={{height: 18, width: 9, marginLeft: 15}}></Image>
        </TouchableOpacity>
      ),
      headerRight: (
        <View style={Styles.rightTitle}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ThongBao');
            }}>
            <Image
              source={require('../images/icon_noti.png')}
              style={{height: 20, width: 17, marginRight: 15}}
              resizeMode="contain"></Image>
            <View style={Styles.dotNoti}></View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Home');
            }}>
            <Image
              source={require('../images/icon_user.png')}
              style={{height: 20, width: 20, marginRight: 15}}
              resizeMode="contain"></Image>
          </TouchableOpacity>
        </View>
      ),
      headerBackground: (
        <View>
          <Image
            source={require('../images/bg_header.png')}
            style={{width: '100%', height: 50}}
          />
          <BoxShadowBottom></BoxShadowBottom>
        </View>
      ),
    }),
  },
);

const MainTabs = createBottomTabNavigator(
  {
    // mainStack: {
    //   screen: mainStack,
    //   navigationOptions: {
    //     tabBarVisible: false,
    //     tabBarLabel: ({tintColor, focused}) =>
    //     focused ? (
    //       <Text style={{width:0, height:0}}>Home</Text>
    //     ) : (
    //       <Text style={{width:0, height:0}}>Home</Text>
    //     ),

    //   },
    // },
    // Home: {
    //   screen: Home,
    //   navigationOptions: {
    //     tabBarVisible: false,
    //     tabBarLabel: null,
    //   },
    // },
    QuanLyVanBan: {
      screen: vanbanStack,
      navigationOptions: {
        tabBarLabel: ({tintColor, focused}) =>
          focused ? (
            <Text style={styles.styleTextActive}>Văn bản</Text>
          ) : (
            <Text style={styles.styleTextInActive}>Văn bản</Text>
          ),
        tabBarIcon: (
          <Image
            source={require('../images/qlvb_1.png')}
            style={{width: 24, height: 17}}
            resizeMode="contain"></Image>
        ),
      },
    },
    QuanLyCongViec: {
      screen: congviecStack,
      navigationOptions: {
        tabBarLabel: ({tintColor, focused}) =>
          focused ? (
            <Text style={styles.styleTextActive}>Công việc</Text>
          ) : (
            <Text style={styles.styleTextInActive}>Công việc</Text>
          ),
        tabBarIcon: (
          <Image
            source={require('../images/qlcv.png')}
            style={{width: 21, height: 17}}
            resizeMode="contain"></Image>
        ),
      },
    },
    HoatDongKT: {
      screen: kiemtoanStack,
      navigationOptions: {
        tabBarLabel: ({tintColor, focused}) =>
          focused ? (
            <Text style={styles.styleTextActive}>Kiểm toán</Text>
          ) : (
            <Text style={styles.styleTextInActive}>Kiểm toán</Text>
          ),
        tabBarIcon: (
          <Image
            source={require('../images/hdkt.png')}
            style={{width: 21, height: 17}}
            resizeMode="contain"></Image>
        ),
      },
    },
    TTCanBo: {
      screen: canboStack,
      navigationOptions: {
        tabBarLabel: ({tintColor, focused}) =>
          focused ? (
            <Text style={styles.styleTextActive}>Cán bộ</Text>
          ) : (
            <Text style={styles.styleTextInActive}>Cán bộ</Text>
          ),
        tabBarIcon: (
          <Image
            source={require('../images/ttcb.png')}
            style={{width: 19, height: 18}}
            resizeMode="contain"></Image>
        ),
      },
    },
    QuanLyCuocHop: {
      screen: cuochopStack,
      navigationOptions: {
        tabBarLabel: ({tintColor, focused}) =>
          focused ? (
            <Text style={styles.styleTextActive}>Cuộc họp</Text>
          ) : (
            <Text style={styles.styleTextInActive}>Cuộc họp</Text>
          ),
        tabBarIcon: (
          <Image
            source={require('../images/qlch.png')}
            style={{width: 19, height: 19}}
            resizeMode="contain"></Image>
        ),
      },
    },
  },
  {
    tabBarOptions: {
      activeTintColor: '#1b335f',
      inactiveTintColor: '#000',
      labelStyle: {
        fontSize: 10,
      },
    },
  },
);
const App = createSwitchNavigator(
  {
    Login: AuthStack,
    AppStack: MainTabs,
    mainStack: mainStack,
  },
  {
    initialRouteName: 'Login',
  },
);
const MyDrawerNavigator = createDrawerNavigator(
  {
    App: {
      screen: App,
    },
  },
  {
    drawerPosition: 'left',
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerWidth: Dimensions.get('window').width - 120,
    contentComponent: props => <SideMenu {...props} />,
  },
);
export default createAppContainer(MyDrawerNavigator);
const styles = StyleSheet.create({
  styleTextActive: {
    fontWeight: 'bold',
    fontSize: 11,
    textAlign: 'center',
    color: '#1b335f',
  },
  styleTextInActive: {
    fontWeight: 'normal',
    fontSize: 11,
    textAlign: 'center',
    color: '#000',
  },
  styleHeaderTitle: {
    color: '#fff',
    // alignSelf: 'center',
    textAlign: 'center',
    flex: 1,
    fontSize: 15,
    // textTransform: 'uppercase',
    fontWeight: 'bold',
  },
});
