import React, {Component} from 'react';
import {
  AppRegistry,
  SectionList,
  View,
  Text,
  TouchableOpacity,
  Image,
  Picker,
  TextInput,
  ScrollView,
  FlatList,
  ImageBackground,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';
import {Styles} from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderTab from './HeaderTab';
import ModalDropdown from 'react-native-modal-dropdown';
import canboData from '../data/canboData';
class BoxShadowBottom extends Component {
  render() {
    return (
      <LinearGradient
        colors={['#d2d2d2', '#fbfbfb']}
        style={Styles.shadow}></LinearGradient>
    );
  }
}
class CanBoItem extends Component {
  render() {
    return (
      <View style={Styles.styleBgCb}>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('ChiTietThongTinCanBo', {
              item: this.props.item,
            })
          }>
          <View
            style={{flexDirection: 'row', marginLeft: 3, marginVertical: 6}}>
            <Image
              source={this.props.item.image}
              style={{width: 92, height: 111, borderRadius: 5}}></Image>
            <View style={{marginLeft: 10, flex: 1}}>
              <View style={Styles.listItemCb}>
                <View style={{flex: 3.5}}>
                  <Text style={Styles.styleTextlabel}>Họ và tên:</Text>
                </View>
                <View style={{flex: 7}}>
                  <Text style={Styles.styleTextlabelName}>
                    {this.props.item.name}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <Image
                    source={require('../images/arrow.png')}
                    style={{width: 14, height: 10}}></Image>
                </View>
              </View>
              <View style={Styles.listItemCb}>
                <Text style={[Styles.styleTextlabel, {flex: 3}]}>
                  Ngày sinh:
                </Text>
                <Text style={[Styles.styleTextlabel1, {flex: 7}]}>
                  {this.props.item.ngaysinh}
                </Text>
              </View>
              <View style={Styles.listItemCb}>
                <Text style={[Styles.styleTextlabel, {flex: 3}]}>Số hiệu:</Text>
                <Text style={[Styles.styleTextlabel1, {flex: 7}]}>
                  {this.props.item.sohieu}
                </Text>
              </View>
              <View style={Styles.listItemCb}>
                <Text style={[Styles.styleTextlabel, {flex: 3}]}>Chức vụ:</Text>
                <Text style={[Styles.styleTextlabel1, {flex: 7}]}>
                  {this.props.item.chucvu}
                </Text>
              </View>
              <View style={Styles.listItemCb}>
                <Text style={[Styles.styleTextlabel, {flex: 3}]}>Đơn vị:</Text>
                <Text style={[Styles.styleTextlabel1, {flex: 7}]}>
                  {this.props.item.tendonvi}
                </Text>
              </View>
              <View style={Styles.listItemCb}>
                <Text style={[Styles.styleTextlabel, {flex: 3}]}>
                  Điện thoại:
                </Text>
                <Text style={[Styles.styleTextlabel1, {flex: 7}]}>
                  {this.props.item.phone}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <BoxShadowBottom></BoxShadowBottom>
      </View>
    );
  }
}
export default class QuanLyCanBo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultValue_dropDown: 'Tìm kiếm theo đơn vị',
      animation: new Animated.Value(0),
    };
  }
  toggleOpen = () => {
    const toValue = this._open ? 0 : 1;
    Animated.timing(this.state.animation, {
      toValue,
      duration: 200,
    }).start();
    this._open = !this._open;
  };
  render() {
    const bgStyle = {
      transform: [
        {
          scale: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 30],
          }),
        },
      ],
    };
    const baocaoStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -60],
          }),
        },
      ],
    };
    const themmoiStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -115],
          }),
        },
      ],
    };
    const canhanStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -170],
          }),
        },
      ],
    };
    const labelPositionInterpolate = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -240],
    });
    const opacityInterpolate = this.state.animation.interpolate({
      inputRange: [0, 0.9, 1],
      outputRange: [0, 0, 1],
    });
    const labelStyle = {
      opacity: opacityInterpolate,
      transform: [
        {
          translateX: labelPositionInterpolate,
        },
      ],
    };
    return (
      <ImageBackground
        source={require('../images/bg_trong.png')}
        style={{height: '100%', width: '100%'}}
        resizeMode="contain">
        <View style={Styles.container}>
          <Animated.View style={[Styles.Overlay, bgStyle]} />
          <HeaderTab
            name="Quản lý cán bộ"
            navigation={this.props.navigation}></HeaderTab>
          <View
            style={[Styles.wrapper, {marginTop: 20, backgroundColor: '#fff'}]}>
            <View style={Styles.wrapperTitle}>
              <Text style={Styles.nameCV}>Thông tin cán bộ</Text>
              <View style={Styles.line}></View>
            </View>
            <View style={{flexDirection: 'column', marginHorizontal: 5}}>
              <View style={Styles.styleBoxDrop}>
                <ScrollView>
                  <ModalDropdown
                    style={Styles.styleDropdown}
                    textStyle={Styles.dropdown_text}
                    defaultValue={this.state.defaultValue_dropDown}
                    dropdownStyle={Styles.dropdown_2_dropdown}
                    options={[
                      'Kiểm toán nhà nước chuyên ngành I',
                      'Kiểm toán nhà nước chuyên ngành II',
                    ]}></ModalDropdown>
                  <Image
                    source={require('../images/down.png')}
                    style={Styles.pickerIcon1}
                    resizeMode="contain"></Image>
                </ScrollView>
              </View>
              <View style={Styles.inputContainer}>
                <TextInput
                  style={[Styles.styleInput, {fontSize: 14}]}
                  placeholder="Họ tên Kiểm toán viên"
                  placeholderTextColor="#6d6161"></TextInput>
              </View>
              <View
                style={{
                  marginTop: 20,
                  marginBottom: 20,
                }}>
                <TouchableOpacity
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 32,
                    marginBottom: 10,
                  }}
                  onPress={() => {
                    // this.props.navigation.navigate('Home');
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    colors={['#00adef', '#005aab']}
                    style={Styles.searchButton}>
                    <Text style={Styles.textSearch}>Tìm kiếm</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </View>
            <BoxShadowBottom></BoxShadowBottom>
            {/* danh sach */}
          </View>
          <FlatList
            data={canboData}
            style={{marginHorizontal: 20, marginTop: 10}}
            renderItem={({item, index}) => {
              return (
                <CanBoItem
                  item={item}
                  index={index}
                  navigation={this.props.navigation}></CanBoItem>
              );
            }}></FlatList>
          <TouchableWithoutFeedback>
            <Animated.View
              style={[Styles.buttonCreat, Styles.otherButton, canhanStyle]}>
                <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ThongKeTongHop');
                }}>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                Thông tin cá nhân
              </Animated.Text>
              <Image
                source={require('../images/canbo_canhan.png')}
                style={{width: 46, height: 46}}></Image>
                </TouchableOpacity>
            </Animated.View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback>
            <Animated.View
              style={[Styles.buttonCreat, Styles.otherButton, themmoiStyle]}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ThongKeTongHop');
                }}>
                <Animated.Text style={[Styles.textToggle, labelStyle]}>
                  Thống kê tổng hợp
                </Animated.Text>
                <Image
                  source={require('../images/canbo_thongke.png')}
                  style={{width: 46, height: 46}}></Image>
              </TouchableOpacity>
            </Animated.View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback>
            <Animated.View
              style={[Styles.buttonCreat, Styles.otherButton, baocaoStyle]}>
                <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ThongKeTongHop');
                }}>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                Cán bộ quản lý các cấp
              </Animated.Text>
              <Image
                source={require('../images/canbo_ql.png')}
                style={{width: 46, height: 46}}></Image>
                </TouchableOpacity>
            </Animated.View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={this.toggleOpen}>
            <View style={Styles.buttonCreat}>
              <Image
                source={require('../images/icon_fix.png')}
                style={{width: 70, height: 70, zIndex: 1}}
                resizeMode="cover"></Image>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ImageBackground>
    );
  }
}
