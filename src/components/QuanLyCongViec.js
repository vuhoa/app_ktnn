import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  Button,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';
import {Styles} from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderTab from './HeaderTab';
import congViecData from '../data/congViecData';
import Overlay from 'react-native-modal-overlay';
import {FloatingAction} from 'react-native-floating-action';

class CongViecItem extends Component {
  render() {
    return (
      <View style={{flexDirection: 'column'}}>
        <View style={[Styles.flexRow, Styles.boxTitleCv]}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#00adef', '#005aab']}
            style={Styles.dot}></LinearGradient>

          <Text style={Styles.nameCV}>{this.props.item.name}</Text>
        </View>
        <View style={Styles.detailCv}>
          <View style={[Styles.flexRow, Styles.space]}>
            <Text style={Styles.styleLabel}>Tên đơn vị đầu mối:</Text>
            <Text style={Styles.styleContent}>{this.props.item.tendonvi}</Text>
          </View>
          <View style={[Styles.flexRow, Styles.space]}>
            <Text style={Styles.styleLabel}>Hạn xử lý:</Text>
            <Text style={Styles.styleContent}>{this.props.item.hanxuly}</Text>
          </View>
          <View style={[Styles.flexRow, Styles.space]}>
            <Text style={Styles.styleLabel}>Trạng thái:</Text>
            {this.props.item.status == 0 ? (
              <Text
                style={[
                  Styles.styleContent,
                  {color: '#f9c33d', paddingLeft: 5},
                ]}>
                Chưa xử lý
              </Text>
            ) : (
              <Text
                style={[
                  Styles.styleContent,
                  {color: '#29aae1', paddingLeft: 5},
                ]}>
                Đang xử lý
              </Text>
            )}
          </View>
        </View>
      </View>
    );
  }
}
export default class QuanLyCongViec extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      animation: new Animated.Value(0),
    };
  }
  showHideComponents = () => {
    this.setState(state => ({
      active: !state.active,
    }));
  };
  toggleOpen = () => {
    const toValue = this._open ? 0 : 1;
    Animated.timing(this.state.animation, {
      toValue,
      duration: 200,
    }).start();
    this._open = !this._open;
  };
  render() {
    const bgStyle = {
      transform: [
        {
          scale: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 30],
          }),
        },
      ],
    };
    const baocaoStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -60],
          }),
        },
      ],
    };
    const themmoiStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -115],
          }),
        },
      ],
    };
    const labelPositionInterpolate = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -240],
    });
    const opacityInterpolate = this.state.animation.interpolate({
      inputRange: [0, 0.9, 1],
      outputRange: [0, 0, 1],
    });
    const labelStyle = {
      opacity: opacityInterpolate,
      transform: [
        {
          translateX: labelPositionInterpolate,
        },
      ],
    };
    return (
      <View style={[Styles.container]}>
        <Animated.View style={[Styles.Overlay, bgStyle]} />
        <HeaderTab
          name="Quản lý công việc"
          navigation={this.props.navigation}></HeaderTab>
        {/* <View style={{marginHorizontal:20}}> */}
        <View
          style={[Styles.wrapperTitle, {paddingTop: 12, marginHorizontal: 20}]}>
          <Text style={Styles.styleTitle}>Danh sách công việc</Text>
          <View style={Styles.line}></View>
        </View>
        <FlatList
          style={[Styles.flaslist, {zIndex: 0, marginHorizontal: 20}]}
          data={congViecData}
          renderItem={({item, index}) => {
            return <CongViecItem item={item} index={index}></CongViecItem>;
          }}></FlatList>
        <LinearGradient
          colors={['#d3d3d3', '#f7f7f7']}
          style={Styles.shadow}></LinearGradient>
        {/* </View> */}
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, themmoiStyle]}>
            <TouchableOpacity>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                Thêm mới công việc
              </Animated.Text>
              <Image
                source={require('../images/qlcv_themmoi.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, baocaoStyle]}>
            <TouchableOpacity>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                Báo cáo công việc
              </Animated.Text>
              <Image
                source={require('../images/qlcv_baocao.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={this.toggleOpen}>
          <View style={Styles.buttonCreat}>
            <Image
              source={require('../images/icon_fix.png')}
              style={{width: 70, height: 70, zIndex: 1}}
              resizeMode="cover"></Image>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
