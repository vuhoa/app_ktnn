import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  FlatList,
  Animated,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {Styles} from '../config/Styles';
import danhsachcuochopData from '../data/danhsachcuochopData';
import HeaderTab from './HeaderTab';
import LinearGradient from 'react-native-linear-gradient';

class BoxShadowBottom extends Component {
  render() {
    return (
      <LinearGradient
        colors={['#d2d2d2', '#fbfbfb']}
        style={Styles.shadow}></LinearGradient>
    );
  }
}
class CuocHopItem extends Component {
  render() {
    return (
      <View style={{marginBottom: 10}}>
        <View style={[Styles.flexRow, Styles.boxTitleCv]}>
          <Image
            source={require('../images/icon_dscuochop.png')}
            style={{width: 14, height: 13, marginRight: 8}}
            resizeMode="contain"></Image>

          <Text style={Styles.nameCV}>{this.props.item.name}</Text>
        </View>
        <View style={[Styles.detailCv, {paddingLeft: 30}]}>
          <View style={[Styles.flexRow, Styles.space,{flexWrap:'wrap'}]}>
            <Text style={Styles.styleLabel}>Thời gian:</Text>
            <Text style={[Styles.styleContent,{flexWrap:'wrap'}]}>{this.props.item.thoigian}</Text>
          </View>
          <View style={[Styles.flexRow, Styles.space]}>
            <Text style={Styles.styleLabel}>Địa điểm:</Text>
            <Text style={Styles.styleContent}>{this.props.item.diadiem}</Text>
          </View>
          <View style={[Styles.flexRow, Styles.space]}>
            <Text style={Styles.styleLabel}>Chủ trì:</Text>
            <Text style={Styles.styleContent}>{this.props.item.chutri}</Text>
          </View>
        </View>
        <BoxShadowBottom></BoxShadowBottom>
      </View>
    );
  }
}

export default class QuanLyCuocHop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      animation: new Animated.Value(0),
    };
  }
  toggleOpen = () => {
    const toValue = this._open ? 0 : 1;
    Animated.timing(this.state.animation, {
      toValue,
      duration: 200,
    }).start();
    this._open = !this._open;
  };
  render() {
    const bgStyle = {
      transform: [
        {
          scale: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 30],
          }),
        },
      ],
    };
    const baocaoStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -60],
          }),
        },
      ],
    };
    const themmoiStyle = {
      transform: [
        {
          scale: this.state.animation,
        },
        {
          translateY: this.state.animation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -115],
          }),
        },
      ],
    };
    const labelPositionInterpolate = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -240],
    });
    const opacityInterpolate = this.state.animation.interpolate({
      inputRange: [0, 0.9, 1],
      outputRange: [0, 0, 1],
    });
    const labelStyle = {
      opacity: opacityInterpolate,
      transform: [
        {
          translateX: labelPositionInterpolate,
        },
      ],
    };
    return (
      <View style={Styles.container}>
        <Animated.View style={[Styles.Overlay, bgStyle]} />
        <HeaderTab
          name="Danh sách cuộc họp"
          navigation={this.props.navigation}></HeaderTab>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
            marginVertical: 15,
          }}>
          <Text style={{color: '#0b5492', fontSize: 12}}>Tuần trước</Text>
          <View style={{flexDirection: 'column', alignItems: 'center'}}>
            <Text style={{color: '#000', fontSize: 14}}>Tuần 44</Text>
            <Text style={{color: '#000', fontSize: 12}}>
              Từ ngày 27/10/2019 - 03/11/2019
            </Text>
          </View>
          <Text style={{color: '#0b5492', fontSize: 12}}>Tuần tiếp theo</Text>
        </View>
        <View style={{marginHorizontal: 20}}>
          <BoxShadowBottom></BoxShadowBottom>
        </View>
        {/* <View style={[Styles.wrapper, {marginVertical: 20}]}> */}
        <FlatList
          style={{marginHorizontal: 20, marginTop: 10}}
          data={danhsachcuochopData}
          renderItem={({item, index}) => {
            return <CuocHopItem item={item} index={index}></CuocHopItem>;
          }}></FlatList>
        {/* </View> */}

        <TouchableWithoutFeedback>
          <Animated.View
            style={[Styles.buttonCreat, Styles.otherButton, baocaoStyle]}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('LichCt');
              }}>
              <Animated.Text style={[Styles.textToggle, labelStyle]}>
                Lịch công tác
              </Animated.Text>
              <Image
                source={require('../images/lct_ldkt.png')}
                style={{width: 46, height: 46}}></Image>
            </TouchableOpacity>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={this.toggleOpen}>
          <View style={Styles.buttonCreat}>
            <Image
              source={require('../images/icon_fix.png')}
              style={{width: 70, height: 70, zIndex: 1}}
              resizeMode="cover"></Image>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
