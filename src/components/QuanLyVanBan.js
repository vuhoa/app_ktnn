import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, ScrollView,ImageBackground} from 'react-native';
import {Styles} from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderTab from './HeaderTab';
import Navigators from './Navigators';
export default class QuanLyVanBan extends Component {
  static navigationOptions = ({navigation}) => {
    // const params = navigation.state.params || {};
    // let tabBarLabel = 'Văn bản';
    // let tabBarIcon = () => (
    //   <Image
    //     source={require('../images/qlvb_1.png')}
    //     style={{width: 20, height: 17}}></Image>
    // );
    // let headerLeft =(
    //   <TouchableOpacity onPress={()=>
    //     {
    //       navigation.goBack()
    //     }
    //     }>
    //     <Image source ={require('../images/back.png')} style={ { height: 18, width: 9, marginLeft: 15}}></Image>
    //   </TouchableOpacity>
    // );
    // let headerTitleStyle = {
    //     color: '#f9e193',
    //     alignSelf:'center',
    //     textAlign: "center",
    //     flex: 1,
    //     fontSize: 15,
    //     textTransform: "uppercase",
    //     fontWeight:'bold'
    //   };
    // let headerStyle={
    //     borderBottomWidth: 1,
    //     borderBottomColor: "#f9e193",
    //     height:51
    // };
    // let headerRight= (
    //     <View style={Styles.rightTitle}>
    //         <TouchableOpacity onPress={()=> {navigation.navigate('Home') }}>
    //         <Image source ={require('../images/icon_noti.png')} style={ { height: 17, width: 16, marginRight: 15}}></Image>
    //         </TouchableOpacity>
    //         <TouchableOpacity onPress={()=> {navigation.navigate('Home') }}>
    //             <Image source ={require('../images/icon_user.png')} style={ { height: 17, width: 17, marginRight: 15}}></Image>
    //         </TouchableOpacity>
    //     </View>

    // );
    // let headerBackground = (
    //     <Image
    //       source={require("../images/bg_header.png")}
    //       style={{ width: "100%", height: 50 }}
    //     />
    //   );
    // return {tabBarLabel, tabBarIcon};
  };

  render() {
    const navigation = this.props.navigation;
 
    return (
      <ImageBackground
        source={require('../images/bg_trong.png')}
        style={{height: '100%', width: '100%'}}
        resizeMode="contain">
      <View style={Styles.containerDetail}>
        
        <HeaderTab
          name="Quản lý văn bản"
          navigation={this.props.navigation}></HeaderTab>
        <ScrollView contentContainerStyle={{ paddingVertical: 12}}>
        <View style={Styles.topDetail}>
          <View style={Styles.wrapper}>
            <View style={Styles.wrapperTitle}>
              {/* <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#fcdb70', '#f6ebab']} style={Styles.gradientTitle}> */}
              <Text style={Styles.styleTitle}>Văn bản đến</Text>
              <View style={Styles.line}></View>
              {/* </LinearGradient> */}
            </View>
            <View style={Styles.boxDetail}>
            <TouchableOpacity onPress={()=>{navigation.navigate('DanhSachVanBan')}}>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/dsvanban.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Danh sách</Text>
                  <Text style={Styles.name}>văn bản</Text>
                </View>
              </TouchableOpacity>
              
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/vbvanthu.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Văn bản đã</Text>
                  <Text style={Styles.name}>chuyển văn thư</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{navigation.navigate('VanBanChuaXuLy')}}>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/vbchuaxl.png')}
                    style={Styles.styleIcon}></Image>
                    <View style={Styles.dotSl}><Text style={{color:'#fff', fontSize:12,alignItems:'center'}}>2</Text></View>
                  <Text style={Styles.name}>Văn bản</Text>
                  <Text style={Styles.name}>chưa xử lý</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/vbquahan.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Văn bản</Text>
                  <Text style={Styles.name}>quá hạn</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={Styles.boxDetail}>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/vbtheodoi.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Văn bản </Text>
                  <Text style={Styles.name}>theo dõi</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/vbuyquyen.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Văn bản</Text>
                  <Text style={Styles.name}>ủy quyền</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/vbchiase.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Văn bản</Text>
                  <Text style={Styles.name}>chia sẻ</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {/* vb di */}
        <View style={Styles.bottomDetail}>
          <View style={Styles.wrapper}>
            <View style={Styles.wrapperTitle}>
              {/* <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#fcdb70', '#f6ebab']} style={Styles.gradientTitle}> */}
              <Text style={Styles.styleTitle}>Văn bản đi</Text>
              <View style={Styles.line}></View>
              {/* </LinearGradient> */}
            </View>
            <View style={Styles.boxDetail}>
            <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/dsvbdi.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Danh sách</Text>
                  <Text style={Styles.name}>văn bản</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/vbdichuaxl.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Văn bản</Text>
                  <Text style={Styles.name}>chưa xử lý</Text>
                </View>
              </TouchableOpacity>
              

              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/vbkyso.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Văn bản</Text>
                  <Text style={Styles.name}>ký sổ</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/tracuuvbcu.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Tra cứu</Text>
                  <Text style={Styles.name}>văn bản cũ</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={Styles.bottomDetail}>
          <View style={Styles.wrapper}>
            <View style={Styles.wrapperTitle}>
              {/* <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#fcdb70', '#f6ebab']} style={Styles.gradientTitle}> */}
              <Text style={Styles.styleTitle}>Tờ trình</Text>
              <View style={Styles.line}></View>
              {/* </LinearGradient> */}
            </View>
            <View style={Styles.boxDetail}>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/totrinhchuaxl.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Chưa </Text>
                  <Text style={Styles.name}>xử lý</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/totrinhduyet.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>LĐ KTNN </Text>
                  <Text style={Styles.name}>duyệt</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/totrinhnoibo.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Trao đổi</Text>
                  <Text style={Styles.name}>nội bộ</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={Styles.item}>
                  <Image
                    source={require('../images/totrinhds.png')}
                    style={Styles.styleIcon}></Image>
                  <Text style={Styles.name}>Danh sách</Text>
                  <Text style={Styles.name}>tờ trình</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </ScrollView>
      </View>
      </ImageBackground>
    );
  }
}
