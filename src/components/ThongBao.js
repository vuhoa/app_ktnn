import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  Button,
} from 'react-native';
import thongbaoData from '../data/thongbaoData';
import {Styles} from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderTab from './HeaderTab';
class BoxShadowBottom extends Component {
    render() {
      return (
        <LinearGradient
          colors={['#d2d2d2', '#fbfbfb']}
          style={Styles.shadow}></LinearGradient>
      );
    }
  }
class ThongBaoItem extends Component {
  render() {
    return (
      <View style={{marginBottom:10}}>
        <View style={[Styles.flexRow, Styles.boxTitleCv]}>
          <Image
            source={require('../images/tb.png')}
            style={{height: 16, width: 15, marginRight:6}} resizeMode="contain"></Image>
          <Text style={Styles.nameCV}>{this.props.item.name}</Text>
        </View>
        <View style={[Styles.flexRow,Styles.detailCv]}>
          <Text style={Styles.styleLabel}>Tổng số: </Text>
          <Text style={Styles.styleNumber}>
            {this.props.item.number} Thông báo
          </Text>
        </View>
        <BoxShadowBottom></BoxShadowBottom>
      </View>
    );
  }
}
export default class ThongBao extends Component {
  render() {
    return (
      <View>
        {/* <HeaderTab
          name="Thông báo"
          navigation={this.props.navigation}></HeaderTab> */}
      <View style={[Styles.wrapper, {marginVertical: 20}]}>
        
        <FlatList
          data={thongbaoData}
          renderItem={({item, index}) => {
            return <ThongBaoItem item={item} index={index}></ThongBaoItem>;
          }}></FlatList>
      </View>     
      </View>
      
    );
  }
}
