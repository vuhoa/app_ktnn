import React, {Component} from 'react';
import {
  AppRegistry,
  SectionList,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Styles} from '../config/Styles';
import LinearGradient from 'react-native-linear-gradient';
import HeaderTab from './HeaderTab';
import {ScrollView} from 'react-native-gesture-handler';
import {
  VictoryPie,
  VictoryBar,
  VictoryChart,
  VictoryTheme,
} from 'victory-native';
const data = [
  {name: 'Tiến sĩ', solieu: 3, color: '#0266ff'},
  {name: 'Thạc sĩ', solieu: 105, color: '#30d2d6'},
  {name: 'Đại học', solieu: 92, color: '#24395b'},
  {name: 'Khác', solieu: 160, color: '#4988a6'},
];
const colorSwitcher = {
  fill: data => {
    var color = 'red';

    if (data.solieu === 3) {
      color = '#0266ff';
    }
    if (data.solieu === 105) {
      color = '#30d2d6';
    }
    if (data.solieu === 92) {
      color = '#24395b';
    }
    if (data.solieu === 160) {
      color = '#4988a6';
    }
    return color;
  },
  strokeWidth: 0,
};
class BoxShadowBottom extends Component {
  render() {
    return (
      <LinearGradient
        colors={['#d3d3d3', '#f7f7f7']}
        style={Styles.shadow}></LinearGradient>
    );
  }
}

export default class ThongKeThongHop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
  }

  // static navigationOptions = ({navigation}) => {
  //   let tabBarLabel = 'Cán bộ';
  //   let tabBarIcon = () => (
  //     <Image
  //       source={require('../images/ttcb.png')}
  //       style={{width: 19, height: 13}}></Image>
  //   );
  //   return {tabBarLabel, tabBarIcon};
  // };

  render() {
    const colorSwitcher: any = {
      fill: (data: any) => {
        let color = 'blue';
  
        if (data.solieu > 0 && data.solieu <= 25) {
          color = 'red';
        }
  
        if (data.solieu > 25 && data.solieu <= 93) {
          color = 'orange';
        }
  
        if (data.solieu > 94 && data.solieu <= 105) {
          color = 'yellow';
        }
  
        if (data.solieu > 105 && data.solieu <= 165) {
          color = 'green';
        }
  
        return color;
      },
      strokeWidth: 0
    };
    return (
      <View style={Styles.container}>
        {/* <HeaderTab
          name="Quản lý cán bộ"
          navigation={this.props.navigation}></HeaderTab> */}
        <ScrollView>
          <View style={[Styles.wrapper, {marginVertical: 15}]}>
            {/* can bo bien che */}
            <View>
              <View>
                <View style={[Styles.flexRow, Styles.bgTitle]}>
                  <View
                    style={[Styles.dot, {backgroundColor: '#0b5491'}]}></View>
                  <Text style={Styles.nameCV}>Cán bộ biên chế</Text>
                </View>
                <View style={Styles.detailCv}>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber}>208 Người</Text>
                  </View>
                  <Text style={Styles.styleLabel1}>Số liệu năm 2019</Text>
                </View>
              </View>

              <View style={Styles.itemList}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Công chức</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>179 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.itemList1}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Viên chức</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>18 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.itemList}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Người lao động</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>11 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  height: 150,
                }}>
                <VictoryPie
                  width={300}
                  height={300}
                  startAngle={90}
                  endAngle={-90}
                  colorScale={['#1ba1c6', '#db3912', '#fe9900']}
                  data={[{y: 179}, {y: 18}, {y: 11}]}
                  labels={() => null}
                />
              </View>
              <BoxShadowBottom></BoxShadowBottom>
            </View>
            {/* Can bo ngach */}
            <View style={{paddingTop: 16}}>
              <View>
                <View style={[Styles.flexRow, Styles.bgTitle]}>
                  <View
                    style={[Styles.dot, {backgroundColor: '#0b5491'}]}></View>
                  <Text style={Styles.nameCV}>Cán bộ theo ngạch</Text>
                </View>
                <View style={Styles.detailCv}>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber}>208 Người</Text>
                  </View>
                  <Text style={Styles.styleLabel1}>Số liệu năm 2019</Text>
                </View>
              </View>

              <View style={Styles.itemList}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Kiểm toán viên cao cấp</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>7 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.itemList1}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Kiểm toán viên chính</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>57 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.itemList}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Chuyên viên chính</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>6 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.itemList1}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Kiểm toán viên</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>107 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.itemList}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Chuyên viên</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>22 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.itemList1}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Khác</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>9 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  height: 150,
                }}>
                <VictoryPie
                  width={300}
                  height={300}
                  startAngle={90}
                  endAngle={-90}
                  colorScale={[
                    '#f3b200',
                    '#f87821',
                    '#fcf74f',
                    '#0b62a9',
                    '#5c5cf7',
                    '#00bdb7',
                  ]}
                  data={[{y: 7}, {y: 57}, {y: 6}, {y: 107}, {y: 22}, {y: 9}]}
                  labels={() => null}
                />
              </View>
              <BoxShadowBottom></BoxShadowBottom>
            </View>
            {/*  */}
            <View style={{paddingTop: 16}}>
              <View>
                <View style={[Styles.flexRow, Styles.bgTitle]}>
                  <View
                    style={[Styles.dot, {backgroundColor: '#0b5491'}]}></View>
                  <Text style={Styles.nameCV}>Trình độ đào tạo</Text>
                </View>
                <View style={Styles.detailCv}>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber}>208 Người</Text>
                  </View>
                  <Text style={Styles.styleLabel1}>Số liệu năm 2019</Text>
                </View>
              </View>

              <View style={Styles.itemList}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Tiến sĩ</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>3 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.itemList1}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Thạc sĩ</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>105 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={Styles.itemList}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Đại học</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>92 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={Styles.itemList1}>
                <TouchableOpacity>
                  <View style={Styles.boxlistTitle}>
                    <Text style={Styles.listTitle}>Khác</Text>
                    <Image
                      source={require('../images/arrow.png')}
                      style={{width: 14, height: 10}}></Image>
                  </View>
                  <View style={[Styles.flexRow, Styles.space]}>
                    <Text style={Styles.styleLabel}>Tổng số: </Text>
                    <Text style={Styles.styleNumber1}>160 Người</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <VictoryChart
                  width={350}
                  theme={VictoryTheme.material}
                  domainPadding={{x: 35}}
                  domain={{y: [0, 250]}}>
                  <VictoryBar
                    // data={[
                    //   {x: 'Tiến sĩ', y: 3, fill: '#0266ff', opacity: 0.2},
                    //   {x: 'Thạc sĩ', y: 105, fill: '#30d2d6', opacity: 0.2},
                    //   {x: 'Đại học', y: 92, fill: '#24395b', opacity: 0.2},
                    //   {x: 'Khác', y: 160, fill: '#4988a6', opacity: 0.2},
                    // ]}
                    data={data}
                    // data={data}
                    x="name"
                    y="solieu"
                    dataAttributes={[
                      {fill: "#0266ff"},
                      {fill: "#30d2d6"},
                      {fill: "#24395b"},
                      {fill: "#4988a6"}
                  ]}
                    // fill= "color"
                    barRatio={0.8}
                    // style={{data: { ...colorSwitcher }}}  
                    // standalone={false}
                    // style={{data: { ...colorSwitcher }}}
                    // style={{
                    //   data: {
                    //     fill: (d) => '#0266ff',
                    //   },
                    // }}
                    // style={{ data:  { fill: (d) => d.fill, opacity: (d) => d.opacity } }}
                  />
                </VictoryChart>
              </View>
              <BoxShadowBottom></BoxShadowBottom>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
