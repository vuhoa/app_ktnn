import React, {Component} from 'react';
import {Text, View, Image, FlatList} from 'react-native';
import {Styles} from '../config/Styles';
import listVbChuaXuLy from '../data/listVbChuaXuly';
import HeaderTab from './HeaderTab';
import LinearGradient from 'react-native-linear-gradient';
class BoxShadowBottom extends Component {
  render() {
    return (
      <LinearGradient
        colors={['#d2d2d2', '#fbfbfb']}
        style={Styles.shadow}></LinearGradient>
    );
  }
}
class VanBanItem extends Component {
  render() {
    return (
      <View style={{marginBottom: 10}}>
        <View style={[Styles.flexRow, Styles.boxTitleCv]}>
            {this.props.item.isRead== 0 ? ( <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#00adef', '#005aab']}
            style={Styles.dot}></LinearGradient>) :(<View style={[Styles.circleMenu,{marginRight:5}]}></View>) }
         
          <Text style={Styles.nameCV}>{this.props.item.name}</Text>
        </View>
        <View style={[Styles.detailCv]}>
          <Text style={[Styles.styleContent, {paddingLeft:0}]}>{this.props.item.description}</Text>
          <View style={[Styles.flexRow]}>
            <View style={[Styles.flexRow]}>
              <Image
                source={require('../images/xemvb.png')}
                style={Styles.iconView} resizeMode="contain"></Image>
              <Text style={[Styles.styleNumber1, {fontStyle: 'italic',fontSize:12}]}>
                Xem văn bản
              </Text>
            </View>
            <View style={[Styles.flexRow]}>
              <Image
                source={require('../images/xulyvb.png')}
                style={Styles.iconView} resizeMode="contain"></Image>
              <Text style={[Styles.styleNumber1, {fontStyle: 'italic',fontSize:12}]}>
                Xử lý văn bản
              </Text>
            </View>
            <View style={[Styles.flexRow]}>
              <Image
                source={require('../images/hanxl.png')}
                style={Styles.iconView} resizeMode="contain"></Image>
              <Text style={[Styles.styleNumber1, {fontStyle: 'italic',fontSize:12}]}>
                Hạn xử lý
              </Text>
            </View>
          </View>
        </View>
        <BoxShadowBottom></BoxShadowBottom>
      </View>
    );
  }
}
export default class VanBanChuaXuLy extends Component {
  render() {
    return (
      <View style={Styles.container}>
        {/* <HeaderTab
          name="Văn bản chưa xử lý"
          navigation={this.props.navigation}></HeaderTab> */}
        <View style={[Styles.wrapper, {marginVertical: 20}]}>
          <FlatList
            data={listVbChuaXuLy}
            renderItem={({item, index}) => {
              return <VanBanItem item={item} index={index}></VanBanItem>;
            }}></FlatList>
        </View>
      </View>
    );
  }
}
