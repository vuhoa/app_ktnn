import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Styles} from '../config/Styles';
// import DropdownMenu from 'react-native-dropdown-menu';
import LinearGradient from 'react-native-linear-gradient';
class BoxShadowBottom extends Component {
  render() {
    return (
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#fbda70', '#f5eaaa']}
        style={{height: 2}}></LinearGradient>
    );
  }
}
export default class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: true,
      isVbdi: false,
      isTotrinh: false,
      isDuthao: false,
      isDsCuocHop:true,
      isLichCt:false
    };
  }
  showHideComponents = () => {
    this.setState(state => ({
      active: !state.active,
    }));
  };
  
  showHideComponentsVbdi = () => {
    this.setState(state => ({
      isVbdi: !state.isVbdi,
    }));
  };
  showHideComponentsTotrinh = () => {
    this.setState(state => ({
      isTotrinh: !state.isTotrinh,
    }));
  };
  showHideComponentsDuthao = () => {
    this.setState(state => ({
      isDuthao: !state.isDuthao,
    }));
  };
  showHideComponentsDsCuocHop = () => {
    this.setState(state => ({
      isDsCuocHop: !state.isDsCuocHop,
    }));
  };
  showHideComponentsLichCt = () => {
    this.setState(state => ({
      isDsCuocHop: !state.isDsCuocHop,
    }));
  };
  
  render() {
    return (
      <View style={Styles.container}>
        <View>
          <ImageBackground
            source={require('../images/bg_menu.png')}
            style={{width: '100%', height: 67, resizeMode: 'contain'}}>
            <View style={[Styles.flexRow, {paddingHorizontal: 10}]}>
              <Image
                source={require('../images/logo1.png')}
                style={{height: 50, width: 50}}
                resizeMode="contain"></Image>
              <Image
                source={require('../images/textMenu.png')}
                style={{width: 150, height: 48, marginLeft: 8}}
                resizeMode="contain"></Image>
            </View>
          </ImageBackground>
          <BoxShadowBottom></BoxShadowBottom>
        </View>
        <ScrollView>
          {/* Quan ly van ban */}
          <View>
            <View style={[Styles.flexRow, Styles.sectionHeadingStyle]}>
              <Image
                source={require('../images/menu_qlcv.png')}
                style={{width: 20, height: 17}}></Image>
              <Text
                style={Styles.nameMenu}
                onPress={() => {
                  this.props.navigation.navigate('QuanLyVanBan');
                }}>
                Quản lý văn bản, điều hành
              </Text>
            </View>
            {/*  */}

            <View>
              <View style={Styles.navSectionStyle}>
                <TouchableOpacity
                  onPress={() => {
                    this.showHideComponents();
                  }}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.flexRow}>
                      <View style={Styles.circleMenu}></View>
                      <Text style={Styles.nameMenu}>Văn bản đến</Text>
                    </View>
                    <Image
                      source={require('../images/down.png')}
                      style={{width: 9, height: 5}}
                      resizeMode="contain"></Image>
                  </View>
                </TouchableOpacity>
              </View>
              {this.state.active ? (
                <View style={Styles.navSectionStyle2}>
                  <Text style={Styles.nameMenuChild}>Văn bản chưa xử lý</Text>
                  <Text style={Styles.nameMenuChild}>
                    Văn bản chuyển văn thư
                  </Text>
                  <Text style={Styles.nameMenuChild}>Danh sách văn bản</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản theo dõi</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản ủy quyền</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản chia sẻ</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản quá hạn</Text>
                  <Text style={Styles.nameMenuChild}>Tra cứu văn bản cũ</Text>
                </View>
              ) : null}
            </View>
            <View>
              <View style={Styles.navSectionStyle}>
                <TouchableOpacity
                  onPress={() => {
                    this.showHideComponentsVbdi();
                  }}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.flexRow}>
                      <View style={Styles.circleMenu}></View>
                      <Text style={Styles.nameMenu}>Văn bản đi</Text>
                    </View>
                    <Image
                      source={require('../images/down.png')}
                      style={{width: 9, height: 5}}
                      resizeMode="contain"></Image>
                  </View>
                </TouchableOpacity>
              </View>
              {this.state.isVbdi ? (
                <View style={Styles.navSectionStyle2}>
                  <Text style={Styles.nameMenuChild}>Văn bản chưa xử lý</Text>
                  <Text style={Styles.nameMenuChild}>
                    Văn bản chuyển văn thư
                  </Text>
                  <Text style={Styles.nameMenuChild}>Danh sách văn bản</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản theo dõi</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản ủy quyền</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản chia sẻ</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản quá hạn</Text>
                  <Text style={Styles.nameMenuChild}>Tra cứu văn bản cũ</Text>
                </View>
              ) : null}
            </View>
            <View>
              <View style={Styles.navSectionStyle}>
                <TouchableOpacity
                  onPress={() => {
                    this.showHideComponentsTotrinh();
                  }}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.flexRow}>
                      <View style={Styles.circleMenu}></View>
                      <Text style={Styles.nameMenu}>Tờ trình</Text>
                    </View>
                    <Image
                      source={require('../images/down.png')}
                      style={{width: 9, height: 5}}
                      resizeMode="contain"></Image>
                  </View>
                </TouchableOpacity>
              </View>
              {this.state.isTotrinh ? (
                <View style={Styles.navSectionStyle2}>
                  <Text style={Styles.nameMenuChild}>Văn bản chưa xử lý</Text>
                  <Text style={Styles.nameMenuChild}>
                    Văn bản chuyển văn thư
                  </Text>
                  <Text style={Styles.nameMenuChild}>Danh sách văn bản</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản theo dõi</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản ủy quyền</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản chia sẻ</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản quá hạn</Text>
                  <Text style={Styles.nameMenuChild}>Tra cứu văn bản cũ</Text>
                </View>
              ) : null}
            </View>
            <View>
              <View style={Styles.navSectionStyle}>
                <TouchableOpacity
                  onPress={() => {
                    this.showHideComponentsDuthao();
                  }}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.flexRow}>
                      <View style={Styles.circleMenu}></View>
                      <Text style={Styles.nameMenu}>Văn bản dự thảo</Text>
                    </View>
                    <Image
                      source={require('../images/down.png')}
                      style={{width: 9, height: 5}}
                      resizeMode="contain"></Image>
                  </View>
                </TouchableOpacity>
              </View>
              {this.state.isDuthao ? (
                <View style={Styles.navSectionStyle2}>
                  <Text style={Styles.nameMenuChild}>Văn bản chưa xử lý</Text>
                  <Text style={Styles.nameMenuChild}>
                    Văn bản chuyển văn thư
                  </Text>
                  <Text style={Styles.nameMenuChild}>Danh sách văn bản</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản theo dõi</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản ủy quyền</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản chia sẻ</Text>
                  <Text style={Styles.nameMenuChild}>Văn bản quá hạn</Text>
                  <Text style={Styles.nameMenuChild}>Tra cứu văn bản cũ</Text>
                </View>
              ) : null}
            </View>
          </View>
          {/* Ql cong viec */}
          <View>
            <View style={[Styles.flexRow, Styles.sectionHeadingStyle]}>
              <Image
                source={require('../images/menu_qlvb.png')}
                style={{width: 17, height: 17}}></Image>
              <Text
                style={Styles.nameMenu}
                onPress={() => {
                  this.props.navigation.navigate('QuanLyCongViec');
                }}>
                Quản lý công việc
              </Text>
            </View>
            {/*  */}
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Thêm mới công việc</Text>
                  </View>
                </View>
              </View>
            </View>
            {/*  */}
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Danh sách công việc</Text>
                  </View>
                </View>
              </View>
            </View>
            {/*  */}
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Báo cáo công việc</Text>
                  </View>
                </View>
              </View>
            </View>
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Quản lý công việc</Text>
                  </View>
                </View>
              </View>
            </View>
            {/*  */}
          </View>
          {/* Hoat dong kiem toán */}
          <View>
            <View style={[Styles.flexRow, Styles.sectionHeadingStyle]}>
              <Image
                source={require('../images/iconKt_menu.png')}
                style={{width: 22, height: 14}}></Image>
              <Text
                style={Styles.nameMenu}
                onPress={() => {
                  this.props.navigation.navigate('QuanLyCongViec');
                }}>
                Hoạt động kiểm toán
              </Text>
            </View>
            {/*  */}
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Theo dõi tiến độ</Text>
                  </View>
                </View>
              </View>
            </View>
            {/*  */}
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Tổng hợp kết quả KT</Text>
                  </View>
                </View>
              </View>
            </View>
            {/*  */}
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Thực hiện kiến nghị KT</Text>
                  </View>
                </View>
              </View>
            </View>
            {/*  */}
          </View>
          {/* Quan ly cuoc hop */}
          <View>
            <View style={[Styles.flexRow, Styles.sectionHeadingStyle]}>
              <Image
                source={require('../images/icon_cuochopMenu.png')}
                style={{width: 19, height: 19}}></Image>
              <Text
                style={Styles.nameMenu}
                onPress={() => {
                  this.props.navigation.navigate('QuanLyCuocHop');
                }}>
                Quản lý cuộc họp
              </Text>
            </View>
            {/*  */}

            <View>
              <View style={Styles.navSectionStyle}>
                <TouchableOpacity
                  onPress={() => {
                    this.showHideComponentsDsCuocHop();
                  }}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.flexRow}>
                      <View style={Styles.circleMenu}></View>
                      <Text style={Styles.nameMenu}>Danh sách cuộc họp</Text>
                    </View>
                    <Image
                      source={require('../images/down.png')}
                      style={{width: 9, height: 5}}
                      resizeMode="contain"></Image>
                  </View>
                </TouchableOpacity>
              </View>
              {this.state.isDsCuocHop ? (
                <View style={Styles.navSectionStyle2}>
                  <Text style={Styles.nameMenuChild}>Lịch công tác tuấn</Text>
                  <Text style={Styles.nameMenuChild}>
                    Lịch công tác tuần các ĐV
                  </Text>
                  <Text style={Styles.nameMenuChild}>
                    Lịch công tác của các ĐV
                  </Text>
                  <Text style={Styles.nameMenuChild}>LCT tháng của LĐKT</Text>
                </View>
              ) : null}
            </View>
            <View>
              <View style={Styles.navSectionStyle}>
                <TouchableOpacity
                  onPress={() => {
                    this.showHideComponentsLichCt();
                  }}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.flexRow}>
                      <View style={Styles.circleMenu}></View>
                      <Text style={Styles.nameMenu}>Lịch công tác</Text>
                    </View>
                    <Image
                      source={require('../images/down.png')}
                      style={{width: 9, height: 5}}
                      resizeMode="contain"></Image>
                  </View>
                </TouchableOpacity>
              </View>
              {this.state.isLichCt ? (
                <View style={Styles.navSectionStyle2}>
                  <Text style={Styles.nameMenuChild}>Lịch công tác tuần</Text>
                  <Text style={Styles.nameMenuChild}>
                    Lịch công tác tuần các ĐV
                  </Text>
                  <Text style={Styles.nameMenuChild}>
                    Lịch công tác của các ĐV
                  </Text>
                  <Text style={Styles.nameMenuChild}>LCT tháng của LĐKT</Text>
                </View>
              ) : null}
            </View>
          
          </View>
          {/* Thong tin can bo */}
          <View>
            <View style={[Styles.flexRow, Styles.sectionHeadingStyle]}>
              <Image
                source={require('../images/icon_cbMenu.png')}
                style={{width: 22, height: 14}}></Image>
              <Text
                style={Styles.nameMenu}
                onPress={() => {
                  this.props.navigation.navigate('TTCanBo');
                }}>
               Thông tin cán bộ 
              </Text>
            </View>
            {/*  */}
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Cán bộ biên chế</Text>
                  </View>
                </View>
              </View>
            </View>
            {/*  */}
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Cán bộ QL các cấp</Text>
                  </View>
                </View>
              </View>
            </View>
            {/*  */}
            <View>
              <View style={Styles.navSectionStyle}>
                <View style={Styles.flexRow}>
                  <View style={Styles.flexRow}>
                    <View style={Styles.circleMenu}></View>
                    <Text style={Styles.nameMenu}>Cán bộ theo ngạch</Text>
                  </View>
                </View>
              </View>
            </View>
            {/*  */}
          </View>
        </ScrollView>
      </View>
    );
  }
}
