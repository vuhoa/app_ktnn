import {Dimensions} from 'react-native';
var HEIGHTSCREEN = Dimensions.get('window').height;
var WIDTHSCREEN = Dimensions.get('window').width;
//  var {height, width} = Dimensions.get('window');
const widthCol = WIDTHSCREEN / 4;
export const Styles = {
  // page home
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems:'stretch'
  },
  top: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#cbcbcb',
    marginBottom: 24,
  },
  topLogin: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 24,
  },
  down: {
    flex: 7,
    flexDirection: 'column',
  },
  boxDown: {
    flexDirection: 'row',
    // alignItems:'center',
    justifyContent: 'space-evenly',
    // justifyContent:'space-evenly',
    marginBottom: 20,
  },
  boxItem: {
    // margin
    textAlign: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },

  styleIcon: {
    height: 65,
    width: 65,
  },
  textName: {
    fontSize: 14,
    color: '#fff',
    textAlign: 'center',
  },
  // title Home
  rightTitle: {
    flexDirection: 'row',
  },
  // style Quan ly van ban
  containerDetail: {
    flexDirection: 'column',
    flex: 1,
    // justifyContent: 'space-between',
  },
  name: {
    color: '#000',
    fontSize: 14,
    textAlign: 'center',
  },
  wrapper: {
    // flex:1,
    flexDirection: 'column',
    // alignItems: 'stretch',
    marginHorizontal: 20,
    // marginTop: 12,
    justifyContent: 'flex-start',
    // shadowColor: '#000',
    // shadownOffset: {width: 0, height: 5},
    // shadowOpacity: 0.2,
    // elevation: 1,
    // borderColor: '#ddd',
    // borderBottomWidth: 3,
  },

  topDetail: {
    // flex:6,
    flexDirection: 'column',
  },
  boxDetail: {
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  item: {
    flexDirection: 'column',
    alignItems: 'center',
    width: widthCol - 10,
    marginBottom: 15,
  },
  styleTitle: {
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'left',
    color: '#4c4c4c',
    paddingVertical: 5,
    // flex:0.5
  },
  gradientTitle: {
    borderRadius: 10,
  },
  wrapperTitle: {
    // backgroundColor:'red',
    // width: WIDTHSCREEN / 3,
    // flex: 1,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // paddingVertical:15
  },
  bottomDetail: {
    // flex:4,
  },
  // Quan ly cong viec
  wrapperTitleCv: {
    width: WIDTHSCREEN / 2,
    marginBottom: 10,
  },
  // header Tab
  wrapperHeader: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderBottomWidth: 2,
    // borderBottomColor: '#f9e193',
    // height: 52,
  },
  headerTextTitle: {
    color: '#fff',
    alignSelf: 'center',
    textAlign: 'center',
    flex: 1,
    fontSize: 15,
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  line: {
    height: 1,
    backgroundColor: '#c4c4c4',
    flex: 2,
    marginLeft: 10,
  },
  flexRow: {
    flexDirection: 'row',
    flex: 1,
    paddingVertical: 5,
    alignItems: 'center',
  },
  styleLabel: {
    fontWeight: 'bold',
    color: '#333',
    fontSize: 14,
  },
  nameCV: {
    fontSize: 15,
    color: '#0b5491',
    fontWeight: 'bold',
  },
  styleContent: {
    fontSize: 14,
    paddingLeft: 5,
  },
  styleContent1: {
    fontSize: 14,
    paddingTop: 5,
  },
  dot: {
    height: 8,
    width: 8,
    borderRadius: 4,
    marginRight: 5,
  },
  boxTitleCv: {
    backgroundColor: '#e2e2e2',
    borderRadius: 4,
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  detailCv: {
    paddingLeft: 23,
    marginBottom: 9,
    marginTop: 4,
  },
  space: {
    paddingVertical: 3,
  },
  space1: {
    paddingVertical: 6,
  },
  paddingTitle: {
    paddingBottom: 12,
    paddingTop: 24,
  },
  shadow: {
    // position:'absolute',
    // bottom:0,
    // left:0,
    height: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    zIndex:-1
  },
  buttonCreat: {
    // width:60,
    // height:60,
    position: 'absolute',
    bottom: 10,
    right: 10,
    alignItems: 'center',
    zIndex: 3,
  },
  toggleBox: {
    position: 'absolute',
    bottom: 60,
    right: 10,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  alignCenter: {
    alignItems: 'center',
  },
  textToggle: {
    fontSize: 14,
    color: '#fff',
    paddingRight: 15,
    position: 'absolute',
    right: -200,
    backgroundColor: 'transparent',
    alignItems: 'flex-end',
    textAlign:'right',
    width:200,
    top:10
  },
  iconFix: {
    position: 'absolute',
    bottom: 10,
    right: 10,
  },

  styleLabel1: {
    color: '#000',
    fontSize: 14,
  },
  bgTitle: {
    backgroundColor: '#b2c4ce',
    borderRadius: 4,
    paddingHorizontal: 10,
  },
  itemHd: {
    flexDirection: 'column',
  },
  styleNumber: {
    color: '#ff0000',
    fontSize: 14,
    alignItems:'center'
  },
  // Hoat dong kiem toan
  itemList: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#f2f2f2',
    borderLeftWidth: 5,
    borderLeftColor: '#989898',
    paddingHorizontal: 10,
    paddingTop: 8,
    paddingBottom: 12,
    // paddingVertical:12
  },
  listTitle: {
    fontSize: 14,
    color: '#333',
    fontWeight: 'bold',
  },
  styleNumber1: {
    color: '#0b5492',
    fontSize: 13,
  },
  boxlistTitle: {
    paddingBottom: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  circle: {
    height: 12,
    width: 12,
    borderRadius: 6,
    marginRight: 9,
  },
  itemList1: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    borderLeftWidth: 5,
    borderLeftColor: '#dddddd',
    paddingHorizontal: 10,
    paddingTop: 8,
    paddingBottom: 12,
  },
  // Login
  textInputContainer: {
    marginHorizontal: 38,
    marginVertical: 10,
    backgroundColor: '#fff',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  textInput: {
    height: 35,
    paddingLeft: 10,
    paddingVertical: 2,
  },
  loginButton: {
    width: 158,
    justifyContent: 'center',
    alignItems: 'center',
    height: 38,
    borderRadius: 18,
  },
  loginTitle: {
    color: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
  },
  forgetPass: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25,
  },
  textforgetPass: {
    color: '#fff',
    fontSize: 14,
    borderBottomColor: '#fff',
    borderBottomWidth: 0.5,
    paddingVertical: 5,
  },
  otherButton: {
    position: 'absolute',
    right: 20,
    bottom: 20,
    flexDirection: 'row',
    alignCenter: 'center',
    justifyContent: 'center',
    // transform: [{
    //   translateY:-70
    // }]
  },
  Overlay: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    width: 50,
    height: 50,
    bottom: 20,
    right: 20,
    borderRadius: 30,
    zIndex: 2,
  },
  Overlay1: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    width: 50,
    height: 50,
    bottom: 20,
    right: -220,
    borderRadius: 30,
    zIndex: 2,
  },
  sectionHeadingStyle: {
    backgroundColor: '#b2c3ce',
    paddingVertical: 12,
    paddingHorizontal: 10,
    fontSize:14
  },
  nameMenu: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000',
    paddingHorizontal: 10,
  },
  circleMenu: {
    height: 8,
    width: 8,
    borderRadius: 4,
    backgroundColor: '#9a9a9a',
  },
  navSectionStyle: {
    paddingLeft: 40,
    backgroundColor: '#edf3ff',
    paddingRight: 10,
  },
  nameMenuChild: {
    fontSize: 14,
    color: '#000',
    paddingVertical: 7,
  },
  navSectionStyle2: {
    paddingLeft: 55,
    backgroundColor: '#fff',
    paddingTop: 5,
    paddingBottom: 15,
  },
  iconView: {
    height: 15,
    width: 14,
    marginRight: 5,
  },
  styleInput: {
    borderColor: '#989898',
    borderWidth: 0.5,
    borderRadius: 4,
    height: 35,
    padding: 5,
    paddingLeft: 12,
    alignItems: 'stretch',
  },
  wrapperContainerInput: {
    // flex:1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputContainer: {
    flex: 1,
    marginHorizontal: 7,
    flexDirection: 'column',
  },
  styleLabelInput: {
    color: '#333',
    fontSize: 14,
    paddingVertical: 10,
  },
  styleSelect: {
    borderColor: '#989898',
    borderWidth: 0.5,
    borderRadius: 4,
    height: 35,
    justifyContent: 'center',
  },

  pickerIcon: {
    height: 9,
    width: 10,
    position: 'absolute',
    right: 8,
  },
  searchButton: {
    width: 128,
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    borderRadius: 15,
    fontSize: 14,
  },
  textSearch: {
    color: '#fff',
    fontSize: 15,
  },
  sectionHeader: {
    flexDirection: 'column',
    marginTop: 15,
  },
  styleDropdown: {
    paddingVertical: 7,
    justifyContent: 'flex-end',
    flex: 1,
  },
  dropdown_text: {
    color: '#6d6161',
    fontSize: 14,
    paddingHorizontal: 10,
  },
  styleBoxDrop: {
    borderColor: '#989898',
    borderWidth: 0.5,
    borderRadius: 4,
    height: 35,
    justifyContent: 'center',
    marginHorizontal: 6,
    marginVertical: 10,
    // flex:1
  },
  dropdown_2_dropdown: {
    marginTop: 10,
    color: '#333',
    flex: 1,
    flexDirection: 'row',
    width: 295,
  },
  pickerIcon1: {
    height: 9,
    width: 10,
    position: 'absolute',
    right: 8,
    top: 15,
  },
  listItemCb: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    paddingVertical:1
    // justifyContent:'space-between'
  },
  styleTextlabel: {
    fontSize: 12,
  },
  styleTextlabel1: {
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'left',
    color: '#333',
  },
  styleTextlabelName: {
    fontSize: 13,
    fontWeight: 'bold',
    color: '#0b5491',
  },
  styleBgCb: {
    backgroundColor: '#fff',
    borderRadius:5,
    marginVertical:5,
    zIndex:4
  },
  flexTitle:{
    flexDirection:'row',
    paddingVertical:5,
    alignItems:'center'
  },
  styleNameCb:{
    fontSize:15,
    fontWeight:'bold',
    color:'#0b5491'
  },
  styleLabelInputSelect:{
    color:'#333',
    fontSize:14,
    paddingLeft:5
  },
  topBoxSelect:{
    marginHorizontal:25,
    marginBottom:10
  },
  detailContent:{
    marginHorizontal:20
  },
  topDetailContent:{
    marginVertical:15,
    borderBottomWidth:0.5,
    borderBottomColor:'#000',
    paddingHorizontal:10
  },
  styleDes:{
    paddingLeft:25,
    marginBottom:10
  },
  triangle:{
    width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 7,
        borderRightWidth: 7,
        borderBottomWidth: 5,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor:"#0b5492",
        transform: [
            { rotate: '180deg' }
        ],
        margin: 0,
        marginLeft: 10,
        borderWidth: 0,
        borderColor:"#fff",
        alignItems:'center',
        position:'absolute',
        bottom:-5
  },
  dotNoti:{
    height:9, width:9,
    backgroundColor:'#eb1c24',
    borderRadius:4.5,
    position:'absolute',
    right:13,
    top:2
    
  },
  dotSl:{
    height:22, width:22,
    backgroundColor:'#eb1c24',
    borderRadius:11,
    position:'absolute',
    right:10,
    borderWidth:0.5,
    borderColor:'#fff',
    alignItems:'center',
    justifyContent:'center'
  }
};
